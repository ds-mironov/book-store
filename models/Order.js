const {Schema, model, Types} = require('mongoose');

const CartSchema = new Schema({
    id: {type: Number, required: true},
    title: {type: String, required: true},
    image: {type: String, required: true},
    price: {type: Number, required: true},
    count: {type: Number, required: true},
    total: {type: Number, required: true},
});

const OrderSchema = new Schema({
    orderNumber: {type: Number, required: true, unique: true},
    cart: [CartSchema],
    billingInfo: {
        address: {type: String, required: true},
        city: {type: String, required: true},
        country: {type: String, required: true},
        email: {type: String, required: true},
        firstName: {type: String, required: true},
        lastName: {type: String, required: true},
        phoneNumber: {type: Number, required: true},
        postalCode: {type: Number, required: true},
    },
    deliveryInfo: {
        deliveryAddress: {type: String},
        deliveryCity: {type: String},
        deliveryPostalCode: {type: Number},
        deliveryState: {type: String},
    },
    delivery: {
        method: {type: String, required: true},
        price: {type: Number, required: true},
    },
    payment: {
        method: {type: String, required: true},
        card: {
            creditNumber: {type: Number, required: true},
            cardHolder: {type: String, required: true},
            expirationDate: {type: Number, required: true},
            cvcCode: {type: Number, required: true},
        },
    },
    additionalInfo: {type: String},
    subscription: {type: Boolean},
    created: {type: Date},
    owner: {type: Types.ObjectId, ref: 'User'},
});

module.exports = model('Order', OrderSchema);
