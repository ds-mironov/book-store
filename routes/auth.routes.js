const { Router } = require('express');
const { check } = require('express-validator');
const AuthController = require('../controllers/auth.controller');

const router = Router();

router.post(
    '/register',
    check('email').isEmail().withMessage('Incorrect email'),
    check('password')
        .isLength({
            min: 6,
        })
        .withMessage('Minimum password length 6 characters'),
    AuthController.register
);

router.post(
    '/login',
    check('email')
        .normalizeEmail()
        .isEmail()
        .withMessage('Please enter a valid email'),
    check('password').exists().withMessage('Please enter your password'),
    AuthController.login
);

module.exports = router;
