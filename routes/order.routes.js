const {Router} = require('express');
const auth = require('../middleware/auth.middleware');
const OrderController = require('../controllers/order.controller');

const router = Router();

router.post('/create', auth, OrderController.create);

module.exports = router;
