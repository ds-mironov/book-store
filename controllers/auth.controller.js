const AuthService = require('../services/auth.service');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('config');

class AuthController {
    async register(req, res) {
        const errors = validationResult(req);
        const {email, password} = req.body;

        if (!errors.isEmpty()) {
            res.status(400).json({
                errors: errors.array(),
                message: 'Incorrect registration data',
            });
        } else {
            try {
                await AuthService.register(email, password);
                res.status(201).json({message: 'User created'});
            } catch (e) {
                if (e.name === 'SpecificError') {
                    res.status(e.code).json({message: e.message});
                    return;
                }
                res.status(500).json({
                    message: e.message,
                });
            }
        }
    }

    async login(req, res) {
        const errors = validationResult(req);
        const {email, password} = req.body;

        if (!errors.isEmpty()) {
            res.status(400).json({
                errors: errors.array(),
                message: 'Incorrect login data',
            });
        } else {
            try {
                const user = await AuthService.login(email, password);

                const token = jwt.sign(
                    {userId: user.id},
                    config.get('authConfig.jwtSecretKey'),
                    {
                        expiresIn: '1h',
                    },
                );

                res.json({token, userId: user.id});
            } catch (e) {
                if (e.name === 'SpecificError') {
                    res.status(e.code).json({message: e.message});
                    return;
                }
                res.status(500).json({
                    message: e.message,
                });
            }
        }
    }
}

module.exports = new AuthController();
