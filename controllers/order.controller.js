const OrderService = require('../services/order.service');

class OrderController {
    async create(req, res) {
        const {orderDetails} = req.body;
        const {userId} = req.user;

        try {
            await OrderService.create(orderDetails, userId);
            res.status(201).json({message: 'Order created'});
        } catch (e) {
            res.status(500).json({
                message: e.message,
            });
        }
    }
}

module.exports = new OrderController();
