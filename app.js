const express = require('express');
const config = require('config');
const mongoose = require('mongoose');
const cors = require('cors');

const corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true,
    optionSuccessStatus: 200,
};

const app = express();

app.use(express.json());
app.use('/api/books', cors(corsOptions));

app.use('/api/auth', require('./routes/auth.routes'));
app.use('/api/order', require('./routes/order.routes'));
app.use('/api/', require('./routes/book.routes'));

const PORT = config.get('dbConfig.port') || 5001;

const runApp = async () => {
    try {
        await mongoose.connect(config.get('dbConfig.mongoUri'), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        app.listen(PORT, () =>
            console.log(`App has been started on port ${PORT}...`),
        );
    } catch (e) {
        console.log('Server startup error', e.message);
        process.exit(1);
    }
};

runApp();
