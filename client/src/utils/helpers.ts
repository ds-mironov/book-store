export function validateEmail(email: string): boolean {
    const regExp =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regExp.test(String(email).toLowerCase());
}

export function decimalAdjustRound(value: string | number, exp?: string | number): number {
    if (typeof exp === 'undefined' || +exp === 0) {
        return Math.round(+value);
    }
    value = +value;
    exp = +exp;

    if (isNaN(value) || !(exp % 1 === 0)) {
        return NaN;
    }

    let chars: string[] = value.toString().split('e');
    const formattedValue = Math.round(+(chars[0] + 'e' + (chars[1] ? +chars[1] - exp : -exp)));

    chars = formattedValue.toString().split('e');
    return +(chars[0] + 'e' + (chars[1] ? +chars[1] + exp : exp));
}

interface hasId {
    id: number;
}

export function toggleButtonText<T extends hasId>(cartItems: T[], bookId: number, initialText: string): string {
    if (!cartItems.length) {
        return initialText;
    } else {
        const itemIndex = cartItems.findIndex(({ id }: {id: number}) => id === bookId);

        if (itemIndex === -1) {
            return initialText;
        } else {
            return 'Added by';
        }
    }
}
