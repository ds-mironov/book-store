import {useCallback, useState} from 'react';

import {useValidation} from './useValidation';

export const useInput = (initialValue: string, validations: object) => {
    const [value, setValue] = useState<string>(initialValue);
    const [isDirty, setDirty] = useState<boolean>(false);
    const valid = useValidation(value, validations);

    const handleChange = useCallback((e) => {
        setValue(e.target.value);
    }, []);

    const handleBlur = useCallback(() => {
        setDirty(true);
    }, []);

    const clearValue = () => {
        setValue('');
    };

    return {
        value,
        handleChange,
        handleBlur,
        isDirty,
        clearValue,
        ...valid,
    } as const;
};
