import {useEffect, useState} from 'react';

import {useLocation} from 'react-router-dom';

const useShowComponentOnPage = (path: string): boolean => {
    const [shouldShowComponent, setShouldShowComponent] = useState(true);
    const location = useLocation();

    useEffect(() => {
        if (location.pathname === path) {
            setShouldShowComponent(false);
        } else {
            setShouldShowComponent(true);
        }
    }, [location]);

    return shouldShowComponent;
};

export default useShowComponentOnPage;
