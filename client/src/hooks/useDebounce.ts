import {useCallback, useRef} from 'react';

export function useDebounce<Params extends never[]>(
    callback: (...args: Params) => never,
    delay: number,
): (...args: Params) => void {
    const timer: {current: NodeJS.Timeout | null} = useRef(null);

    return useCallback(
        (...args) => {
            if (timer.current) {
                clearTimeout(timer.current);
            }

            timer.current = setTimeout(() => {
                callback(...args);
            }, delay);
        },
        [callback, delay],
    );
}
