import {useEffect} from 'react';

export const useHiddenScroll = (isShow: boolean) => {
    useEffect(() => {
        setHidden(isShow);
    }, [isShow]);

    const setHidden = (isOpen: boolean) => {
        const body: HTMLBodyElement | null = document.querySelector('body');

        if (body) {
            body.style.overflow = isOpen ? 'hidden' : 'auto';
        }
    };
};
