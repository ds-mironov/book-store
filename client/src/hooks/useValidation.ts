import {useEffect, useState} from 'react';

import {validateEmail} from '../utils/helpers';

export const useValidation = (value: string, validations: object) => {
    const [isEmpty, setEmpty] = useState(true);
    const [minLengthError, setMinLengthError] = useState(false);
    const [maxLengthError, setMaxLengthError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [inputValid, setInputValid] = useState(false);

    useEffect(() => {
        for (const validation in validations) {
            switch (validation) {
                case 'isEmail':
                    validateEmail(value)
                        ? setEmailError(false)
                        : setEmailError(true);
                    break;
                case 'minLength':
                    value.length < validations[validation as keyof object]
                        ? setMinLengthError(true)
                        : setMinLengthError(false);
                    break;
                case 'isEmpty':
                    value ? setEmpty(false) : setEmpty(true);
                    break;
                case 'maxLength':
                    value.length > validations[validation as keyof object]
                        ? setMaxLengthError(true)
                        : setMaxLengthError(false);
                    break;
                default:
                    break;
            }
        }
    }, [value, validations]);

    useEffect(() => {
        if (isEmpty || minLengthError || maxLengthError || emailError) {
            setInputValid(false);
        } else {
            setInputValid(true);
        }
    }, [isEmpty, minLengthError, maxLengthError, emailError]);

    const clearErrors = () => {
        setEmpty(false);
        setMinLengthError(false);
        setMaxLengthError(false);
        setEmailError(false);
    };

    return {
        isEmpty,
        minLengthError,
        maxLengthError,
        emailError,
        inputValid,
        clearErrors,
    } as const;
};
