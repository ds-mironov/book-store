import React, {ChangeEventHandler, useState} from 'react';

import AdditionalInfo from '../../components/additionalInfo/AdditionalInfo';
import BillingInfo from '../../components/billingInfo/BillingInfo';
import BillingMethod from '../../components/billingMethod/BillingMethod';
import Confirmation from '../../components/confirmation/Confirmation';
import DeliveryAddress from '../../components/deliveryAddress/DeliveryAddress';
import OrderSummary from '../../components/orderSummary/OrderSummary';
import PaymentMethod from '../../components/paymentMethod/PaymentMethod';
import Checkbox from '../../components/UI/checkbox/Checkbox';
import {ICartItem} from '../../types/ShoppingCart';
import './CheckoutPage.css';

type CheckoutPageProps = {
    cart: ICartItem[];
};

function CheckoutPage({cart}: CheckoutPageProps) {
    const [isCheckedAddress, setCheckedAddress] = useState(false);

    const changeAddressHandler: ChangeEventHandler<HTMLInputElement> = (e) => {
        setCheckedAddress(e.target.checked);
    };

    return (
        <div className="content__checkout">
            <div className="checkout__partition">
                <div className="partition__requisites">
                    <div className="requisites__requisites-point">
                        <div className="requisites-point__requisite-title">
                            <h3 className="requisite-title__title">
                                Billing info
                            </h3>
                            <div className="requisite-title__help">
                                <span>Please enter your billing info</span>
                                <span className="requisite-title__step">
                                    Step 1 of 5
                                </span>
                            </div>
                        </div>
                        <BillingInfo />
                        <Checkbox changeAddressHandler={changeAddressHandler}>Ship to a different address?</Checkbox>
                        {isCheckedAddress && <DeliveryAddress />}
                    </div>
                    <div className="requisites__requisites-point">
                        <div className="requisites-point__requisite-title">
                            <h3 className="requisite-title__title">
                                Billing method
                            </h3>
                            <div className="requisite-title__help">
                                <span>Please enter your payment method</span>
                                <span className="requisite-title__step">
                                    Step 2 of 5
                                </span>
                            </div>
                        </div>
                        <BillingMethod />
                    </div>
                    <div className="requisites__requisites-point">
                        <div className="requisites-point__requisite-title">
                            <h3 className="requisite-title__title">
                                Payment method
                            </h3>
                            <div className="requisite-title__help">
                                <span>Please enter your payment method</span>
                                <span className="requisite-title__step">
                                    Step 3 of 5
                                </span>
                            </div>
                        </div>
                        <PaymentMethod />
                    </div>
                    <div className="requisites__requisites-point">
                        <div className="requisites-point__requisite-title">
                            <h3 className="requisite-title__title">
                                Additional information
                            </h3>
                            <div className="requisite-title__help">
                                <span>
                                    Need something else? We will make it for
                                    you!
                                </span>
                                <span className="requisite-title__step">
                                    Step 4 of 5
                                </span>
                            </div>
                        </div>
                        <AdditionalInfo />
                    </div>
                    <div className="requisites__requisites-point">
                        <div className="requisites-point__requisite-title">
                            <h3 className="requisite-title__title">
                                Confirmation
                            </h3>
                            <div className="requisite-title__help">
                                <span>
                                    We are getting to the end. Just few clicks
                                    and your order si ready!
                                </span>
                                <span className="requisite-title__step">
                                    Step 5 of 5
                                </span>
                            </div>
                        </div>
                        <Confirmation cart={cart} />
                    </div>
                </div>
                <div className="partition__order">
                    <OrderSummary cart={cart} />
                </div>
            </div>
        </div>
    );
}

export default CheckoutPage;
