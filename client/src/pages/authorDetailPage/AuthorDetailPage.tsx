import React, {useEffect} from 'react';

import {useParams} from 'react-router-dom';

import BookGrid from '../../components/bookGrid/BookGrid';
import ErrorIndicator from '../../components/shared/errorIndicator/ErrorIndicator';
import Spinner from '../../components/UI/spinner/Spinner';
import {useAppDispatch, useAppSelector} from '../../hooks/redux';
import {getSoleAuthor} from '../../redux/reducers/soleAuthorSlice';
import {IAuthor} from '../../types/Author';
import {ICartItem} from '../../types/ShoppingCart';
import '../bookDetailPage/BookDetailPage.css';
import './AuthorDetailPage.css';

type AuthorDetailPageProps = {
    cart: ICartItem[];
};

function AuthorDetailPage({cart}: AuthorDetailPageProps) {
    const {authorId} = useParams();
    const {author, loading, error} = useAppSelector((state) => state.author);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (authorId != null) {
            dispatch(getSoleAuthor(authorId));
        }
    }, [authorId]);

    const errorIndicator = error ? <ErrorIndicator /> : null;
    const spinner = loading ? <Spinner /> : null;
    const content = !(loading || error || !author) ? (
        <View author={author} loading={loading} error={error} cart={cart} />
    ) : null;

    return (
        <>
            {errorIndicator}
            {spinner}
            {content}
        </>
    );
}

type ViewProps = {
    author: IAuthor;
    loading: boolean;
    error: string;
    cart: ICartItem[];
};

const View = ({author, loading, error, cart}: ViewProps) => {
    const {name, biography, image, books} = author;

    return (
        <>
            <div className="content__product-area">
                <div className="product-area__image-block">
                    <img
                        className="image-block__image"
                        src={image}
                        alt={name}
                    />
                </div>
                <div className="product-area__detail-block">
                    <h1 className="detail-block__title">{name}</h1>
                    <div className="detail-block__description">{biography}</div>
                </div>
            </div>
            <div className="product-area__book-list">
                <BookGrid
                    books={books}
                    loading={loading}
                    error={error}
                    cart={cart}
                />
            </div>
        </>
    );
};

export default AuthorDetailPage;
