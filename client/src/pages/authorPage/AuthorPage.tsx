import React, {useEffect} from 'react';

import AuthorGrid from '../../components/athorGrid/AuthorGrid';
import CategoryInformer from '../../components/categoryInformer/CategoryInformer';
import ErrorBoundary from '../../components/shared/errorBoundary/ErrorBoundary';
import {useAppSelector, useAppDispatch} from '../../hooks/redux';
import {fetchAuthors} from '../../redux/reducers/authorsSlice';
import './AuthorPage.css';

function AuthorPage() {
    const {authors, loading, error} = useAppSelector((state) => state.authors);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (authors.length === 0) {
            dispatch(fetchAuthors());
        }
    }, [authors.length]);

    return (
        <>
            <CategoryInformer quantity={authors.length} category="Authors" />
            <div className="content__category-area">
                <div className="category-area__authors">
                    <ErrorBoundary>
                        <AuthorGrid
                            category={authors}
                            error={error}
                            loading={loading}
                        />
                    </ErrorBoundary>
                </div>
            </div>
        </>
    );
}

export default AuthorPage;
