import React from 'react';

import './Page404.css';
import image from '../../assets/images/not-found.jpg';

function Page404() {
    return (
        <div className="container__not-found-page">
            <img src={image} alt="error 404" />
        </div>
    );
}

export default Page404;
