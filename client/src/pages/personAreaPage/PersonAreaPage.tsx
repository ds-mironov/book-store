import React, {useContext} from 'react';

import {useNavigate} from 'react-router-dom';

import Sandbox from '../../components/sandbox/Sandbox';
import {Button} from '../../components/UI/button/Button';
import {AuthContext, IAuthContext} from '../../contexts/AuthContext';
import './PersonAreaPage.css';

type PersonAreaPageProps = {
    isAuth: boolean;
};

function PersonAreaPage({isAuth}: PersonAreaPageProps) {
    const {logout} = useContext(AuthContext) as IAuthContext;
    const navigate = useNavigate();

    const logoutHandler = () => {
        logout();
        navigate('/');
    };

    if (!isAuth) {
        return null;
    }

    return (
        <div className="container__person-area">
            <div className="person-area__person-header">
                <h1 className="person-header__main-title">Person Area</h1>
                <Button styleClass="to-checkout" handleClick={logoutHandler}>
                    Sing Out
                </Button>
            </div>
            <div className="person-area__sandbox">
                <Sandbox />
            </div>
        </div>
    );
}

export default PersonAreaPage;
