import React from 'react';

import BookGrid from '../../components/bookGrid/BookGrid';
import HomeMenu from '../../components/homeMenu/HomeMenu';
import ErrorBoundary from '../../components/shared/errorBoundary/ErrorBoundary';
import './HomePage.css';
import {IBook} from '../../types/Book';
import {ICartItem} from '../../types/ShoppingCart';

type HomePageProps = {
    books: IBook[];
    loading: boolean;
    error: string | null;
    cart: ICartItem[];
};

function HomePage({books, loading, error, cart}: HomePageProps) {
    return (
        <div className="content__category">
            <ErrorBoundary>
                <HomeMenu books={books} />
            </ErrorBoundary>
            <ErrorBoundary>
                <BookGrid
                    books={books.slice(0, 3)}
                    loading={loading}
                    error={error}
                    cart={cart}
                />
            </ErrorBoundary>
        </div>
    );
}

export default HomePage;
