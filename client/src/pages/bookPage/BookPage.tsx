import React from 'react';

import BookGrid from '../../components/bookGrid/BookGrid';
import CategoryInformer from '../../components/categoryInformer/CategoryInformer';
import CategoryMenu from '../../components/categoryMenu/CategoryMenu';
import CategoryPaginator from '../../components/categoryPaginator/CategoryPaginator';
import ErrorBoundary from '../../components/shared/errorBoundary/ErrorBoundary';
import {IBook} from '../../types/Book';
import {ICartItem} from '../../types/ShoppingCart';
import './BookPage.css';

type BookPageProps = {
    books: IBook[];
    totalBooks: number;
    totalPages: number;
    page: number;
    onChangePage: (page: number) => void;
    loading: boolean;
    error: string | null;
    cart: ICartItem[];
};

function BookPage({
    books,
    totalBooks,
    totalPages,
    page,
    onChangePage,
    loading,
    error,
    cart,
}: BookPageProps) {
    return (
        <>
            <CategoryInformer quantity={totalBooks} category="Books" />
            <div className="content__category-area">
                <div className="category-area__category">
                    <CategoryMenu books={books} />
                    <ErrorBoundary>
                        <BookGrid
                            books={books}
                            loading={loading}
                            error={error}
                            cart={cart}
                        />
                    </ErrorBoundary>
                </div>
                <CategoryPaginator
                    quantity={totalBooks}
                    totalPages={totalPages}
                    page={page}
                    onChangePage={onChangePage}
                />
            </div>
        </>
    );
}

export default BookPage;
