import React, {
    ChangeEvent,
    ChangeEventHandler,
    useEffect,
    useState,
} from 'react';

import {useParams} from 'react-router-dom';

import addSimpleIcon from '../../assets/icons/ic-actions-add-simple.svg';
import BookDetailSection from '../../components/bookDetailSection/BookDetailSection';
import QuantitySelector from '../../components/quantitySelector/QuantitySelector';
import ErrorIndicator from '../../components/shared/errorIndicator/ErrorIndicator';
import {Button} from '../../components/UI/button/Button';
import Spinner from '../../components/UI/spinner/Spinner';
import {useAppDispatch, useAppSelector} from '../../hooks/redux';
import {bookAddedToCart} from '../../redux/reducers/shoppingCartSlice';
import {getSoleBook} from '../../redux/reducers/soleBookSlice';
import {IBook} from '../../types/Book';
import {ICartItem} from '../../types/ShoppingCart';
import {toggleButtonText} from '../../utils/helpers';
import './BookDetailPage.css';

interface IParams {
    readonly bookId?: string;
}

type BookDetailPageProps = {
    cart: ICartItem[];
};

function BookDetailPage({cart}: BookDetailPageProps) {
    const {bookId}: IParams = useParams();
    const {book, loading, error} = useAppSelector((state) => state.book);
    const dispatch = useAppDispatch();
    const [piecesBooks, setPiecesBooks] = useState<string>('1');
    const buttonText = bookId
        ? toggleButtonText<ICartItem>(cart, +bookId, 'Add to cart')
        : 'Add to cart';

    useEffect(() => {
        if (bookId && bookId.length === 13) {
            dispatch(getSoleBook(bookId));
        }
    }, [bookId]);

    const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
        if (+e.target.value >= 0) {
            setPiecesBooks(e.target.value);
        }
    };

    const handleBookAddedToCart = () => {
        if (+piecesBooks > 0 && book) {
            dispatch(bookAddedToCart({book, piecesBooks}));
        }
    };

    const errorIndicator = error ? <ErrorIndicator /> : null;
    const spinner = loading ? <Spinner /> : null;
    const content = !(loading || error || !book) ? (
        <View
            book={book}
            piecesBooks={piecesBooks}
            handleChange={handleChange}
            handleBookAddedToCart={handleBookAddedToCart}
            buttonText={buttonText}
        />
    ) : null;

    return (
        <>
            {errorIndicator}
            {spinner}
            {content}
        </>
    );
}

type ViewProps = {
    book: IBook;
    piecesBooks: string;
    handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
    handleBookAddedToCart: () => void;
    buttonText: string;
};

const View = ({
    book,
    piecesBooks,
    handleChange,
    handleBookAddedToCart,
    buttonText,
}: ViewProps) => {
    const {
        ISBN,
        author,
        title,
        summary,
        image,
        price: {currency, displayValue},
    } = book;

    return (
        <div className="content__product-area">
            <div className="product-area__image-block">
                <img className="image-block__image" src={image} alt={title} />
            </div>
            <div className="product-area__detail-block">
                <h1 className="detail-block__title">{title}</h1>
                <div className="detail-block__description">{summary}</div>
                <BookDetailSection isbn={ISBN} author={author} />
                <div className="detail-block__product-actions">
                    <span className="product-actions__price">
                        {`${displayValue} ${currency}`}
                    </span>
                    <div className="product-actions__choice-quantity">
                        <QuantitySelector
                            quantity={piecesBooks}
                            handleChange={handleChange}
                        />
                    </div>
                    <Button
                        hasBeforeIcon={true}
                        styleClass={'to-cart'}
                        handleClick={handleBookAddedToCart}>
                        <img
                            className="add-btn__plus-icon"
                            src={addSimpleIcon}
                            alt="Add icon"
                        />
                        {buttonText}
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default BookDetailPage;
