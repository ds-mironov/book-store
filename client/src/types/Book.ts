export interface IBook {
    ISBN: number;
    title: string;
    author: string;
    summary: string;
    image: string;
    price: {
        currency: string;
        value: number;
        displayValue: string;
    };
}

export interface IBookState {
    book: IBook | null;
    loading: boolean;
    error: string;
}

export interface IBooksResponse {
    books: IBook[];
    totalBooks: number;
    totalPages: number;
}

export interface IBooksState extends IBooksResponse{
    loading: boolean;
    error: string;
}
