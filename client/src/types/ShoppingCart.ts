import { IBook } from './Book';

export interface ICartItem {
    count: number;
    id: number;
    image: string;
    price: number;
    title: string;
    total: number;
}

export interface IShoppingCartState {
    cartItems: ICartItem[];
}

export interface IAddedPayload {
    book: IBook;
    piecesBooks?: string;
}

export interface IChangedPayload {
    bookUnit: ICartItem;
    quantity: string;
}
