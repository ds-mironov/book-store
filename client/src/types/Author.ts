import {IBook} from './Book';

export interface IAuthor {
    id: string;
    name: string;
    biography: string;
    image: string;
    books?: IBook[];
}

export interface AuthorState {
    authors: IAuthor[];
    loading: boolean;
    error: string;
}

export interface AuthorData {
    authors: IAuthor[];
    links?: object[];
}

export interface SoleAuthorState {
    author: IAuthor | null;
    loading: boolean;
    error: string;
}

export interface SomeAuthorData {
    author: IAuthor;
    links?: object[];
}
