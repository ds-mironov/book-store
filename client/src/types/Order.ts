export interface IOrderState {
    billingInfo: IBillingInfo | null;
    deliveryInfo?: IDeliveryInfo | null;
    delivery: IDelivery | null;
    payment: IPayment | null;
    additionalInfo?: string | null;
}

export interface IBillingInfo {
    address: string;
    city: string;
    country: string;
    email: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    postalCode: string;
}

export interface IDeliveryInfo {
    deliveryAddress: string;
    deliveryCity: string;
    deliveryPostalCode: string;
    deliveryState: string;
}

export interface IDelivery {
    method: string;
    price: number;
}

export interface IPayment {
    card: ICreditCard;
    method: string;
}

export interface ICreditCard {
    creditNumber: string;
    cardHolder: string;
    expirationDate: string;
    cvcCode: string;
}
