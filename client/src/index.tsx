import React from 'react';

import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';

import App from './app/App';
import ScrollToTop from './components/scrollToTop/ScrollToTop';
import ErrorBoundary from './components/shared/errorBoundary/ErrorBoundary';
import {store} from './redux/store';
import './index.css';

render(
    <React.StrictMode>
        <ErrorBoundary>
            <Router>
                <ScrollToTop />
                <Provider store={store}>
                    <App />
                </Provider>
            </Router>
        </ErrorBoundary>
    </React.StrictMode>,
    document.getElementById('root'),
);
