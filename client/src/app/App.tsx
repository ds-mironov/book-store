import React, {useEffect, useState, lazy, Suspense} from 'react';

import {Route, Routes, useNavigate} from 'react-router-dom';

import AuthForm from '../components/authForm/AuthForm';
import Footer from '../components/footer/Footer';
import {FooterLinkSection} from '../components/footerLinkSection/FooterLinkSection';
import FooterTagCloud from '../components/footerTagCloud/FooterTagCloud';
import Header from '../components/header/Header';
import NavMenu from '../components/navMenu/NavMenu';
import SearchPanel from '../components/searchPanel/SearchPanel';
import ShoppingCart from '../components/shoppingCart/ShoppingCart';
import Spinner from '../components/UI/spinner/Spinner';
import {AuthContext, IAuthContext} from '../contexts/AuthContext';
import {useAppDispatch, useAppSelector} from '../hooks/redux';
import {useAuth} from '../hooks/useAuth';
import {useHiddenScroll} from '../hooks/useHiddenScroll';
import useShowComponentOnPage from '../hooks/useShowComponentOnPage';
import BookPage from '../pages/bookPage/BookPage';
import HomePage from '../pages/homePage/HomePage';
import {getBooks} from '../redux/reducers/booksSlice';
import {IBook} from '../types/Book';
import './App.css';

const BookDetailPage = lazy(
    () => import('../pages/bookDetailPage/BookDetailPage'),
);
const AuthorPage = lazy(() => import('../pages/authorPage/AuthorPage'));
const AuthorDetailPage = lazy(
    () => import('../pages/authorDetailPage/AuthorDetailPage'),
);
const PersonAreaPage = lazy(
    () => import('../pages/personAreaPage/PersonAreaPage'),
);
const CheckoutPage = lazy(() => import('../pages/checkoutPage/CheckoutPage'));
const Page404 = lazy(() => import('../pages/page404/Page404'));

function App() {
    const {token, login, logout, userId} = useAuth() as IAuthContext;
    const isAuthenticated = !!token;
    const dispatch = useAppDispatch();
    const cart = useAppSelector((state) => state.shoppingCart.cartItems);
    const {books, totalBooks, totalPages, error, loading} = useAppSelector(
        (state) => state.books,
    );
    const limit = 9;
    const [page, setPage] = useState(1);
    const [showAuthForm, setShowAuthForm] = useState(false);
    const [showCart, setShowCart] = useState(false);
    const [term, setTerm] = useState('');
    useHiddenScroll(showCart);
    useHiddenScroll(showAuthForm);
    const navigate = useNavigate();
    const quantityCartItems = cart.reduce((acc, currentItem) => {
        return acc + currentItem.count;
    }, 0);
    const shouldShowFooter = useShowComponentOnPage('/checkout');

    useEffect(() => {
        dispatch(getBooks({limit, page}));
    }, [page]);

    const searchBook = (items: IBook[], term: string): IBook[] => {
        if (term.length === 0) {
            return items;
        }

        return items.filter((item) => {
            return item.title.indexOf(term) > -1;
        });
    };

    const displayedBooks = searchBook(books, term);

    const onToggleAuthForm = (): void => {
        if (isAuthenticated) {
            navigate('/account');
        } else {
            setShowAuthForm((showAuthForm) => !showAuthForm);
        }
    };

    const onToggleCart = (): void => {
        setShowCart((showCart) => !showCart);
    };

    const onChangeSearch = (term: string): void => {
        setTerm(term);
    };

    const onChangePage = (page: number): void => {
        setPage(page);
    };

    return (
        <AuthContext.Provider
            value={{token, userId, login, logout, isAuthenticated}}>
            <div className="container">
                <Header
                    onToggleAuthForm={onToggleAuthForm}
                    onToggleCart={onToggleCart}
                    quantityCartItems={quantityCartItems}>
                    <SearchPanel onChangeSearch={onChangeSearch} />
                </Header>
                <NavMenu />
                <main className="container__content">
                    <Suspense fallback={<Spinner />}>
                        <Routes>
                            <Route
                                path="/"
                                element={
                                    <HomePage
                                        books={displayedBooks}
                                        loading={loading}
                                        error={error}
                                        cart={cart}
                                    />
                                }
                            />
                            <Route
                                path="books"
                                element={
                                    <BookPage
                                        books={displayedBooks}
                                        totalBooks={totalBooks}
                                        totalPages={totalPages}
                                        onChangePage={onChangePage}
                                        page={page}
                                        loading={loading}
                                        error={error}
                                        cart={cart}
                                    />
                                }
                            />
                            <Route
                                path="book/:bookId"
                                element={<BookDetailPage cart={cart} />}
                            />
                            <Route path="authors" element={<AuthorPage />} />
                            <Route
                                path="author/:authorId"
                                element={<AuthorDetailPage cart={cart} />}
                            />
                            <Route
                                path="account"
                                element={
                                    <PersonAreaPage isAuth={isAuthenticated} />
                                }
                            />
                            <Route
                                path="checkout"
                                element={<CheckoutPage cart={cart} />}
                            />
                            <Route path="*" element={<Page404 />} />
                        </Routes>
                    </Suspense>
                </main>
                <Footer>
                    {shouldShowFooter && <FooterLinkSection />}
                    {shouldShowFooter && <FooterTagCloud bookList={books} />}
                </Footer>
                <AuthForm
                    showAuthForm={showAuthForm}
                    onToggleAuthForm={onToggleAuthForm}
                />
                <ShoppingCart
                    showCart={showCart}
                    onToggleCart={onToggleCart}
                    cart={cart}
                />
            </div>
        </AuthContext.Provider>
    );
}

export default App;
