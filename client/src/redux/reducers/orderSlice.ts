import {createSlice, PayloadAction} from '@reduxjs/toolkit';

import {
    IBillingInfo,
    IDelivery,
    IDeliveryInfo,
    IOrderState,
    IPayment,
} from '../../types/Order';

const initialState = {
    billingInfo: null,
    deliveryInfo: null,
    delivery: null,
    payment: null,
    additionalInfo: null,
} as IOrderState;

export const orderSlice = createSlice({
    name: 'order',
    initialState,
    reducers: {
        addBillingInfoToOrder: (
            state,
            action: PayloadAction<IBillingInfo | null>,
        ) => {
            state.billingInfo = action.payload;
        },
        addDeliveryInfoToOrder: (
            state,
            action: PayloadAction<IDeliveryInfo | null>,
        ) => {
            state.deliveryInfo = action.payload;
        },
        addDeliveryToOrder: (
            state,
            action: PayloadAction<IDelivery | null>,
        ) => {
            state.delivery = action.payload;
        },
        addPaymentToOrder: (state, action: PayloadAction<IPayment | null>) => {
            state.payment = action.payload;
        },
        addAdditionalInfoToOrder: (
            state,
            action: PayloadAction<string | null>,
        ) => {
            state.additionalInfo = action.payload;
        },
    },
});

export const {
    addBillingInfoToOrder,
    addDeliveryInfoToOrder,
    addDeliveryToOrder,
    addPaymentToOrder,
    addAdditionalInfoToOrder,
} = orderSlice.actions;

export default orderSlice.reducer;
