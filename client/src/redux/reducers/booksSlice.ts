import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';

import {IBooksResponse, IBooksState} from '../../types/Book';

const initialState = {
    books: [],
    totalBooks: 0,
    totalPages: 0,
    loading: false,
    error: '',
} as IBooksState;

export const getBooks = createAsyncThunk(
    'books/getBooks',
    async ({limit, page}: {limit: number; page: number}, {rejectWithValue}) => {
        const url = 'http://localhost:5001/api/books';

        try {
            const response = await fetch(`${url}?limit=${limit}&page=${page}`);

            if (!response.ok) {
                rejectWithValue(
                    `Could not fetch ${url}, status: ${response.status}`,
                );
                return;
            }

            return (await response.json()) as IBooksResponse;
        } catch (e) {
            return rejectWithValue((e as Error).message);
        }
    },
);

export const booksSlice = createSlice({
    name: 'books',
    initialState,
    reducers: {},
    extraReducers: {
        [getBooks.pending.type]: (state) => {
            state.loading = true;
        },
        [getBooks.fulfilled.type]: (
            state,
            action: PayloadAction<IBooksResponse>,
        ) => {
            state.loading = false;
            state.error = '';
            state.books = [...state.books, ...action.payload.books];
            state.totalBooks = action.payload.totalBooks;
            state.totalPages = action.payload.totalPages;
        },
        [getBooks.rejected.type]: (state, action: PayloadAction<string>) => {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default booksSlice.reducer;
