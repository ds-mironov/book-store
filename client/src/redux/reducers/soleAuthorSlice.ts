import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';

import {IAuthor, SoleAuthorState, SomeAuthorData} from '../../types/Author';

const initialState = {
    author: null,
    loading: false,
    error: '',
} as SoleAuthorState;

export const getSoleAuthor = createAsyncThunk(
    'author/getSoleAuthor',
    async (authorId: string, {rejectWithValue}) => {
        const url = 'https://private-f12c3-bookstore.apiary-mock.com/authors/';

        try {
            const response = await fetch(`${url}${authorId}`);

            if (!response.ok) {
                rejectWithValue(
                    `Could not fetch ${url}, status: ${response.status}`,
                );
                return;
            }

            const data = (await response.json()) as SomeAuthorData;
            return data.author;
        } catch ({message}) {
            return rejectWithValue(message);
        }
    },
);

export const soleAuthorSlice = createSlice({
    name: 'author',
    initialState,
    reducers: {},
    extraReducers: {
        [getSoleAuthor.pending.type]: (state) => {
            state.loading = true;
        },
        [getSoleAuthor.fulfilled.type]: (
            state,
            action: PayloadAction<IAuthor>,
        ) => {
            state.loading = false;
            state.error = '';
            state.author = action.payload;
        },
        [getSoleAuthor.rejected.type]: (
            state,
            action: PayloadAction<string>,
        ) => {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default soleAuthorSlice.reducer;
