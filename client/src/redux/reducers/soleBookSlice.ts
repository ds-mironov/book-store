import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';

import {IBook, IBookState} from '../../types/Book';

const initialState = {
    book: null,
    loading: false,
    error: '',
} as IBookState;

export const getSoleBook = createAsyncThunk(
    'book/getSoleBook',
    async (bookId: string, {rejectWithValue}) => {
        const url = 'http://localhost:5001/api/books/';

        try {
            const response = await fetch(`${url}${bookId}`);

            if (!response.ok) {
                rejectWithValue(
                    `Could not fetch ${url}, status: ${response.status}`,
                );
                return;
            }

            return (await response.json()) as IBook;
        } catch (e) {
            return rejectWithValue((e as Error).message);
        }
    },
);

export const soleBookSlice = createSlice({
    name: 'book',
    initialState,
    reducers: {},
    extraReducers: {
        [getSoleBook.pending.type]: (state) => {
            state.loading = true;
        },
        [getSoleBook.fulfilled.type]: (state, action: PayloadAction<IBook>) => {
            state.loading = false;
            state.error = '';
            state.book = action.payload;
        },
        [getSoleBook.rejected.type]: (state, action: PayloadAction<string>) => {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default soleBookSlice.reducer;
