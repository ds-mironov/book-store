import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';

import {IAuthor, AuthorState, AuthorData} from '../../types/Author';

const initialState = {
    authors: [],
    loading: true,
    error: '',
} as AuthorState;

export const fetchAuthors = createAsyncThunk(
    'authors/getAuthors',
    async (_, {rejectWithValue}) => {
        const url = 'https://private-f12c3-bookstore.apiary-mock.com/authors';
        try {
            const response = await fetch(url);

            if (!response.ok) {
                rejectWithValue(
                    `Could not fetch ${url}, status: ${response.status}`,
                );
                return;
            }

            const data = (await response.json()) as AuthorData;
            return data.authors;
        } catch (e) {
            return rejectWithValue((e as Error).message);
        }
    },
);

export const authorsSlice = createSlice({
    name: 'authors',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchAuthors.pending.type]: (state) => {
            state.loading = true;
        },
        [fetchAuthors.fulfilled.type]: (
            state,
            action: PayloadAction<IAuthor[]>,
        ) => {
            state.loading = false;
            state.error = '';
            state.authors = action.payload;
        },
        [fetchAuthors.rejected.type]: (
            state,
            action: PayloadAction<string>,
        ) => {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default authorsSlice.reducer;
