import {createSlice, PayloadAction} from '@reduxjs/toolkit';

import {
    IAddedPayload,
    ICartItem,
    IChangedPayload,
    IShoppingCartState,
} from '../../types/ShoppingCart';
import {decimalAdjustRound} from '../../utils/helpers';

const initialState = {
    cartItems: [],
} as IShoppingCartState;

const updateCartItems = (
    cartItems: ICartItem[],
    item: ICartItem,
    idx: number,
) => {
    if (idx === -1) {
        return [...cartItems, item];
    }

    return [...cartItems.slice(0, idx), item, ...cartItems.slice(idx + 1)];
};

export const shoppingCartSlice = createSlice({
    name: 'shoppingCart',
    initialState,
    reducers: {
        bookAddedToCart: (state, action: PayloadAction<IAddedPayload>) => {
            const {book, piecesBooks = '1'} = action.payload;
            const {
                ISBN: bookId,
                title,
                image,
                price: {value},
            } = book;
            const itemIndex = state.cartItems.findIndex(
                ({id}) => id === bookId,
            );
            const item = state.cartItems[itemIndex];
            const newItem = {
                id: bookId,
                title,
                image,
                price: value,
                count: item ? item.count + +piecesBooks : +piecesBooks,
                total: item
                    ? item.total + decimalAdjustRound(value * +piecesBooks, -2)
                    : value,
            };

            state.cartItems = updateCartItems(
                state.cartItems,
                newItem,
                itemIndex,
            );
        },
        bookChangedQuantityCart: (
            state,
            action: PayloadAction<IChangedPayload>,
        ) => {
            const {bookUnit, quantity} = action.payload;
            const itemIndex = state.cartItems.findIndex(
                (cartItem) => cartItem.id === bookUnit.id,
            );
            const newItem = {
                ...bookUnit,
                count: +quantity,
                total: decimalAdjustRound(bookUnit.price * +quantity, -2),
            };

            if (newItem.count < 0) {
                state.cartItems.splice(itemIndex, 1);
            } else {
                state.cartItems = updateCartItems(
                    state.cartItems,
                    newItem,
                    itemIndex,
                );
            }
        },
        bookRemovedFromCart: (state, action: PayloadAction<number>) => {
            const unitId = action.payload;
            const itemIndex = state.cartItems.findIndex(
                (cartItem) => cartItem.id === unitId,
            );
            state.cartItems.splice(itemIndex, 1);
        },
    },
});

export const {bookAddedToCart, bookChangedQuantityCart, bookRemovedFromCart} =
    shoppingCartSlice.actions;

export default shoppingCartSlice.reducer;
