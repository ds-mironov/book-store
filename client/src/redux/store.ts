import {configureStore} from '@reduxjs/toolkit';

import authorsReducer from './reducers/authorsSlice';
import booksReducer from './reducers/booksSlice';
import orderReducer from './reducers/orderSlice';
import shoppingCartReducer from './reducers/shoppingCartSlice';
import soleAuthorReducer from './reducers/soleAuthorSlice';
import soleBookReducer from './reducers/soleBookSlice';

export const store = configureStore({
    reducer: {
        books: booksReducer,
        book: soleBookReducer,
        authors: authorsReducer,
        author: soleAuthorReducer,
        shoppingCart: shoppingCartReducer,
        order: orderReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
    devTools: process.env.NODE_ENV !== 'production',
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
