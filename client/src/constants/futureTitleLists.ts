export interface IFutureTitle {
    title: string;
}

export const futureTitleList1: IFutureTitle[] = [
    {title: 'Author'},
    {title: 'ISBN'},
    {title: 'Stock'},
    {title: 'Publisher'},
];
export const futureTitleList2: IFutureTitle[] = [
    {title: 'Publishing'},
    {title: 'Category'},
    {title: 'Characters'},
    {title: 'Scene'},
];
