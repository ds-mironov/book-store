export interface IFooterSectionLink {
    title: string;
    link: string;
}

export const touchList: IFooterSectionLink[] = [
    {title: 'About Us', link: '*'},
    {title: 'Careers', link: '*'},
    {title: 'Press Releases', link: '*'},
    {title: 'Blog', link: '*'},
];

export const connectionList: IFooterSectionLink[] = [
    {title: 'Facebook', link: 'https://facebook.com/'},
    {title: 'Twitter', link: 'https://twitter.com/'},
    {title: 'Instagram', link: 'https://instagram.com/'},
    {title: 'Youtube', link: 'https://youtube.com/'},
    {title: 'LinkedIn', link: 'https://linkedin.com/'},
];

export const earningList: IFooterSectionLink[] = [
    {title: 'Become an Affiliate', link: '*'},
    {title: 'Advertise your product', link: '*'},
    {title: 'Sell on Market', link: '*'},
];

export const accountList: IFooterSectionLink[] = [
    {title: 'Your account', link: '/account'},
    {title: 'Returns Centre', link: '*'},
    {title: '100 % purchase protection', link: '*'},
    {title: 'Chat with us', link: '*'},
    {title: 'Help', link: '*'},
];
