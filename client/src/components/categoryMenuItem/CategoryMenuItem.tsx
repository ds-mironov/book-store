import React from 'react';
import './CategoryMenuItem.css';

type CategoryMenuItemProps = {
    name: string;
    count: number;
};

function CategoryMenuItem({name, count}: CategoryMenuItemProps) {
    return (
        <>
            <p className="menu-item__link menu-item__link_no-link">{name}</p>
            <div className="menu-item__item-quantity">
                <span>{count}</span>
            </div>
        </>
    );
}

export default CategoryMenuItem;
