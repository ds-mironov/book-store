import React, {useEffect} from 'react';

import {useInput} from '../../hooks/useInput';
import {ICreditCard} from '../../types/Order';
import './CreditCardForm.css';

type CreditCardProps = {
    getCardDetails: (card: ICreditCard) => void;
};

function CreditCardForm({getCardDetails}: CreditCardProps) {
    const creditNumber = useInput('', {
        isEmpty: true,
    });
    const cardHolder = useInput('', {
        isEmpty: true,
    });
    const expire = useInput('', {
        isEmpty: true,
    });
    const cvcCode = useInput('', {
        isEmpty: true,
    });

    useEffect(() => {
        if (
            creditNumber.value &&
            cardHolder.value &&
            expire.value &&
            cvcCode.value
        ) {
            const card = {
                creditNumber: creditNumber.value,
                cardHolder: cardHolder.value,
                expirationDate: expire.value,
                cvcCode: cvcCode.value,
            };
            getCardDetails(card);
        }
    }, [
        creditNumber.value,
        cardHolder.value,
        expire.value,
        cvcCode.value,
    ]);

    return (
        <form className="requisites-point__payment-form">
            <div className="billing-forms__input-block">
                <label className="input-block__label" htmlFor="credit-number">
                    Credit number
                </label>
                <input
                    className="input-block__input"
                    value={creditNumber.value}
                    onChange={creditNumber.handleChange}
                    type="tel"
                    id="credit-number"
                    placeholder="Credit number"
                    name="number"
                    tabIndex={17}
                />
            </div>
            <div className="payment-form__inputs-block">
                <div className="inputs-block__card-holder">
                    <label className="input-block__label" htmlFor="card-holder">
                        Card holder
                    </label>
                    <input
                        className="input-block__input"
                        value={cardHolder.value}
                        onChange={cardHolder.handleChange}
                        type="text"
                        id="card-holder"
                        placeholder="Card holder"
                        name="card-holder"
                        tabIndex={18}
                    />
                </div>
                <div className="inputs-block__expire-date">
                    <label className="input-block__label" htmlFor="expire-date">
                        Expiration date
                    </label>
                    <input
                        className="input-block__input"
                        value={expire.value}
                        onChange={expire.handleChange}
                        type="tel"
                        id="expire-date"
                        placeholder="MM/YY"
                        name="expire-date"
                        tabIndex={19}
                    />
                </div>
                <div className="inputs-block__card-cvc">
                    <label className="input-block__label" htmlFor="input-cvc">
                        CVC
                    </label>
                    <input
                        className="input-block__input"
                        value={cvcCode.value}
                        onChange={cvcCode.handleChange}
                        type="text"
                        id="input-cvc"
                        placeholder="CVC"
                        name="input-cvc"
                        tabIndex={20}
                    />
                </div>
            </div>
        </form>
    );
}

export default CreditCardForm;
