import React from 'react';

import {Link} from 'react-router-dom';
import './FooterTagCloudItem.css';

type FooterTagCloudItemProps = {
    title: string;
    id: number;
};

function FooterTagCloudItem({title, id}: FooterTagCloudItemProps) {
    return (
        <Link to={`book/${id}`} className="tag__link">
            {title}
        </Link>
    );
}

export default FooterTagCloudItem;
