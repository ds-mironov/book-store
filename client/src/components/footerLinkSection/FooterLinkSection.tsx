import React from 'react';

import {
    touchList,
    connectionList,
    earningList,
    accountList,
} from '../../constants/footerLinkSectionLists';
import FooterLinkSectionList from '../footerLinkSectionList/FooterLinkSectionList';
import './FooterLinkSection.css';

export function FooterLinkSection() {
    return (
        <div className="footer__links-block">
            <div className="links-block__touch">
                <h3 className="touch__title">Get in touch</h3>
                <FooterLinkSectionList linkList={touchList} external={false} />
            </div>
            <div className="links-block__connections">
                <h3 className="touch__title">Connections</h3>
                <FooterLinkSectionList linkList={connectionList} external />
            </div>
            <div className="links-block__earning">
                <h3 className="touch__title">Earnings</h3>
                <FooterLinkSectionList
                    linkList={earningList}
                    external={false}
                />
            </div>
            <div className="links-block__account">
                <h3 className="touch__title">Account</h3>
                <FooterLinkSectionList
                    linkList={accountList}
                    external={false}
                />
            </div>
        </div>
    );
}
