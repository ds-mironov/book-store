import React, {ChangeEventHandler, useEffect, useState} from 'react';

import dhlLogo from '../../assets/logo/dhl.svg';
import fedexLogo from '../../assets/logo/fedex.svg';
import {useAppDispatch} from '../../hooks/redux';
import {addDeliveryToOrder} from '../../redux/reducers/orderSlice';
import './BillingMethod.css';

interface IDeliveryOptions {
    FedEx: number;
    DHL: number;
}

const deliveryOptions: IDeliveryOptions = {
    FedEx: 32,
    DHL: 15,
};

function BillingMethod() {
    const dispatch = useAppDispatch();
    const [method, setMethod] = useState<string>('');

    const choiceDelivery: ChangeEventHandler<HTMLInputElement> = (e) => {
        setMethod(e.target.value);
    };

    useEffect(() => {
        if (method) {
            const price = deliveryOptions[method as keyof IDeliveryOptions];
            dispatch(addDeliveryToOrder({method, price}));
        } else {
            dispatch(addDeliveryToOrder(null));
        }
    }, [method]);

    return (
        <>
            <label className="requisites-point__delivery-point">
                <p className="delivery-point__point-detail">
                    <span className="point-detail__title">FedEx</span>
                    <span className="point-detail__price-section">
                        <span className="price-section__price">+32 EUR</span>
                        Additional price
                    </span>
                    <img
                        className="point-detail__logo"
                        src={fedexLogo}
                        alt="Fedex"
                    />
                </p>
                <input
                    className="delivery-point__input"
                    value="FedEx"
                    type="radio"
                    name="delivery"
                    onChange={choiceDelivery}
                />
                <span className="delivery-point__checkmark">{}</span>
            </label>
            <label className="requisites-point__delivery-point  requisites-point__delivery-point_margin_top">
                <p className="delivery-point__point-detail">
                    <span className="point-detail__title">DHL</span>
                    <span className="point-detail__price-section">
                        <span className="price-section__price">+15 EUR</span>
                        Additional price
                    </span>
                    <img src={dhlLogo} alt="DHL" />
                </p>
                <input
                    className="delivery-point__input"
                    value="DHL"
                    type="radio"
                    name="delivery"
                    onChange={choiceDelivery}
                />
                <span className="delivery-point__checkmark">{}</span>
            </label>
        </>
    );
}

export default BillingMethod;
