import React from 'react';

import {nanoid} from '@reduxjs/toolkit';

import {IFooterSectionLink} from '../../constants/footerLinkSectionLists';
import FooterLinkSectionListItem from '../footerLinkSectionListItem/FooterLinkSectionListItem';

type FooterLinkSectionListProps = {
    linkList: IFooterSectionLink[];
    external: boolean;
};

function FooterLinkSectionList({
    linkList,
    external,
}: FooterLinkSectionListProps) {
    return (
        <ul className="touch__touch-list">
            {linkList.map((item) => {
                return (
                    <li key={nanoid()} className="touch-list__item">
                        <FooterLinkSectionListItem
                            title={item.title}
                            link={item.link}
                            external={external}
                        />
                    </li>
                );
            })}
        </ul>
    );
}

export default FooterLinkSectionList;
