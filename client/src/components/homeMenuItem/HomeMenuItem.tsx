import React from 'react';

import {Link} from 'react-router-dom';

import {IBook} from '../../types/Book';

type HomeMenuItemProps = {
    book: IBook;
};

function HomeMenuItem({book}: HomeMenuItemProps) {
    const {ISBN, title} = book;

    return (
        <Link to={`book/${ISBN}`} className="menu-item__link">
            {title}
        </Link>
    );
}

export default HomeMenuItem;
