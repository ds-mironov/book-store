import React, {ReactElement} from 'react';

import classNames from 'classnames';
import './TabBarItem.css';

export type TabBarItemProps = {
    children: ReactElement | null;
    label: string;
    activeTab?: string | null;
};

function TabBarItem({
    children = null,
    label,
    activeTab = null,
    ...attrs
}: TabBarItemProps) {
    const classes = classNames('tab-bar-item', {active: activeTab === label});

    return (
        <div className={classes} {...attrs}>
            {children}
        </div>
    );
}

export default TabBarItem;
