import React from 'react';
import './categoryInformer.css';

type CategoryInformerProps = {
    quantity: number;
    category: string;
};

function CategoryInformer({quantity, category}: CategoryInformerProps) {
    return (
        <div className="content__category-info">
            <h1 className="category-info__title">{category}</h1>
            <div>
                <span className="category-info__count">{quantity}</span>
                <span className="category-info__items">{category}</span>
            </div>
        </div>
    );
}

export default CategoryInformer;
