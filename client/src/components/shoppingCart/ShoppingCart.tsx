import React from 'react';

import {Link, useNavigate} from 'react-router-dom';

import {ICartItem} from '../../types/ShoppingCart';
import {decimalAdjustRound} from '../../utils/helpers';
import ListBookCart from '../listBookCart/ListBookCart';
import {Button} from '../UI/button/Button';
import CloseModalButton from '../UI/closeModalButton/CloseModalButton';
import './ShoppingCart.css';

type ShoppingCartProps = {
    showCart: boolean;
    onToggleCart: () => void;
    cart: ICartItem[];
};

function ShoppingCart({showCart, onToggleCart, cart}: ShoppingCartProps) {
    const navigate = useNavigate();
    const total = decimalAdjustRound(
        cart.reduce((acc, currentItem) => {
            return acc + currentItem.total;
        }, 0),
        -2,
    );

    if (!showCart) {
        return null;
    }

    const onGoToCheckout = () => {
        onToggleCart();
        navigate('/checkout');
    };

    return (
        <div className="container__cart-modal" onClick={onToggleCart}>
            <div
                className="cart-modal__content"
                onClick={(e) => e.stopPropagation()}>
                <div className="content__cart-detail">
                    <div className="cart-detail__title-block">
                        <h1 className="title-block__title">Shopping cart</h1>
                        <CloseModalButton onCloseModal={onToggleCart} />
                    </div>
                    <ListBookCart cart={cart} />
                </div>
                <div className="cart-modal__subtotal-actions">
                    <p className="subtotal-actions__total">Subtotal</p>
                    <p className="subtotal-actions__price">{total} EUR</p>
                    <hr className="subtotal-actions__line" />
                    <div className="subtotal-actions__cart-actions">
                        <Link
                            to="/books"
                            className="cart-actions__link"
                            onClick={onToggleCart}>
                            Continue shopping
                        </Link>
                        <Button
                            disabled={cart.length === 0}
                            styleClass="to-checkout"
                            handleClick={onGoToCheckout}>
                            Go to Checkout
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ShoppingCart;
