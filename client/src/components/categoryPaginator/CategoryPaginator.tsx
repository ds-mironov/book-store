import React from 'react';

import downWhiteIcon from '../../assets/icons/ic-chevron-down-white.svg';
import {Button} from '../UI/button/Button';
import Pagination from '../UI/pagination/Pagination';
import './CategoryPaginator.css';

type CategoryPaginatorProps = {
    quantity: number;
    totalPages: number;
    page: number;
    onChangePage: (page: number) => void;
};

function CategoryPaginator({
    quantity,
    totalPages,
    page,
    onChangePage,
}: CategoryPaginatorProps) {
    const handleChangePage = () => {
        if (page < totalPages) {
            onChangePage(page + 1);
        }
    };

    return (
        <div className="category__pagination">
            <div className="pagination__choice-page">
                <span className="category-info__items category-info__items_padding-none">
                    Page:
                </span>
                <Pagination totalPages={totalPages} page={page} />
            </div>
            <Button styleClass="more-books" handleClick={handleChangePage}>
                Show more products
                <img
                    className="buy-btn__image"
                    src={downWhiteIcon}
                    alt="Arrow down"
                />
            </Button>
            <div>
                <span className="category-info__count">{quantity}</span>
                <span className="category-info__items">Books</span>
            </div>
        </div>
    );
}

export default CategoryPaginator;
