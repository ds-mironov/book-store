import React from 'react';
import './Footer.css';

type FooterProps = {
    children: React.ReactNode;
};

function Footer({children}: FooterProps) {
    return (
        <footer className="container__footer">
            {children}
            <div className="footer__copyright">
                <span>Copyright © 2020 bookstore.site</span>
            </div>
        </footer>
    );
}

export default Footer;
