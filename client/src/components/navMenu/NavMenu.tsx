import React from 'react';

import {Link} from 'react-router-dom';

import downIcon from '../../assets/icons/ic-chevron-down.svg';
import './NavMenu.css';

function NavMenu() {
    return (
        <nav className="header__nav-block">
            <ul className="nav-block__horizontal-menu">
                <li className="horizontal-menu__menu-item">
                    <Link to="books" className="menu-item__menu-link">
                        Books
                        <img
                            className="horizontal-menu__icon"
                            src={downIcon}
                            alt="Arrow down"
                        />
                    </Link>
                </li>
                <li className="horizontal-menu__item">
                    <Link to="authors" className="menu-item__menu-link">
                        Authors
                        <img
                            className="horizontal-menu__icon"
                            src={downIcon}
                            alt="Arrow down"
                        />
                    </Link>
                </li>
            </ul>
        </nav>
    );
}

export default NavMenu;
