import React, {useContext, useEffect, useState, memo} from 'react';

import {useNavigate} from 'react-router-dom';

import {AuthContext, IAuthContext} from '../../contexts/AuthContext';
import {useHttp} from '../../hooks/useHttp';
import {useInput} from '../../hooks/useInput';
import {Button} from '../UI/button/Button';
import CloseModalButton from '../UI/closeModalButton/CloseModalButton';
import Popup from '../UI/popup/Popup';
import './AuthForm.css';

type AuthFormProps = {
    showAuthForm: boolean;
    onToggleAuthForm: () => void;
};

function AuthForm({showAuthForm, onToggleAuthForm}: AuthFormProps) {
    const [modalActive, setModalActive] = useState(false);
    const [message, setMessage] = useState('');
    const navigate = useNavigate();
    const {loading, request, error, clearError} = useHttp();
    const {login} = useContext(AuthContext) as IAuthContext;
    const email = useInput('', {
        isEmpty: true,
        minLength: 3,
        isEmail: true,
        maxLength: 16,
    });
    const password = useInput('', {
        isEmpty: true,
        minLength: 6,
        maxLength: 12,
    });

    useEffect(() => {
        if (error || message) {
            setModalActive(true);
            setTimeout(() => {
                setModalActive(false);
                setMessage('');
                clearError();
            }, 2000);
        }
    }, [error, message]);

    const clearFormFields = () => {
        email.clearValue();
        email.clearErrors();
        password.clearValue();
        password.clearErrors();
    };

    const registerHandler = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        const data = await request('/api/auth/register', 'POST', {
            email: email.value,
            password: password.value,
        });

        if (data?.message) {
            setMessage(data.message);
            setTimeout(() => {
                clearFormFields();
                onToggleAuthForm();
            }, 2000);
        }
    };

    const loginHandler = async (e: React.SyntheticEvent) => {
        e.preventDefault();

        const data = await request('/api/auth/login', 'POST', {
            email: email.value,
            password: password.value,
        });

        if (data?.token && data?.userId) {
            login(data.token, data.userId);
            clearFormFields();
            onToggleAuthForm();
            navigate('/account');
        }
    };

    const handleCloseAuth = () => {
        clearFormFields();
        onToggleAuthForm();
    };

    if (!showAuthForm) {
        return null;
    }

    return (
        <>
            <div className="container__auth" onClick={handleCloseAuth}>
                <form
                    className="auth__form"
                    onClick={(e) => e.stopPropagation()}>
                    <div className="form__close">
                        <CloseModalButton onCloseModal={handleCloseAuth} />
                    </div>
                    <h1 className="form__title">Sign In / SingUp</h1>
                    <div className="form__field">
                        <label className="field__label" htmlFor="input-email">
                            Email
                        </label>
                        <input
                            className="field__input"
                            type="email"
                            id="input-email"
                            placeholder="Enter your email"
                            name="email"
                            tabIndex={1}
                            autoComplete="email"
                            value={email.value}
                            onBlur={email.handleBlur}
                            onChange={email.handleChange}
                        />
                        {email.isDirty && email.isEmpty && (
                            <div className="field__warning">
                                <span>The field must not be empty</span>
                            </div>
                        )}
                        {email.isDirty && email.minLengthError && (
                            <div className="field__warning">
                                <span>Incorrect length</span>
                            </div>
                        )}
                        {email.isDirty && email.maxLengthError && (
                            <div className="field__warning">
                                <span>Email too long</span>
                            </div>
                        )}
                        {email.isDirty && email.emailError && (
                            <div className="field__warning">
                                <span>Incorrect email</span>
                            </div>
                        )}
                    </div>
                    <div className="form__field">
                        <label
                            className="field__label"
                            htmlFor="input-password">
                            Password
                        </label>
                        <input
                            className="field__input"
                            type="password"
                            id="input-password"
                            placeholder="Enter password"
                            name="password"
                            tabIndex={2}
                            autoComplete="new-password"
                            value={password.value}
                            onBlur={password.handleBlur}
                            onChange={password.handleChange}
                        />
                        {password.isDirty && password.isEmpty && (
                            <div className="field__warning">
                                <span>The field must not be empty</span>
                            </div>
                        )}
                        {password.isDirty && password.minLengthError && (
                            <div className="field__warning">
                                <span>Too short password</span>
                            </div>
                        )}
                        {password.isDirty && password.maxLengthError && (
                            <div className="field__warning">
                                <span>Password too long</span>
                            </div>
                        )}
                    </div>
                    <div className="form__action">
                        <Button
                            styleClass="to-checkout"
                            disabled={
                                loading ||
                                !email.inputValid ||
                                !password.inputValid
                            }
                            handleSubmitClick={loginHandler}
                            tabIndex={3}>
                            Sign In
                        </Button>
                        <Button
                            styleClass="to-checkout"
                            disabled={
                                loading ||
                                !email.inputValid ||
                                !password.inputValid
                            }
                            handleSubmitClick={registerHandler}
                            tabIndex={4}>
                            Sing Up
                        </Button>
                    </div>
                </form>
            </div>
            <Popup active={modalActive} setActive={setModalActive}>
                <span>{error || message}</span>
            </Popup>
        </>
    );
}

export default memo(AuthForm);
