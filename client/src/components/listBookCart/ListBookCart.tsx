import React from 'react';

import {ICartItem} from '../../types/ShoppingCart';
import ListBookCartItem from '../listBookCartItem/ListBookCartItem';
import './ListBookCart.css';

type ListBookCartProps = {
    cart: ICartItem[];
};

function ListBookCart({cart}: ListBookCartProps) {
    if (!cart.length) {
        return <h4 className="product-detail__message">Cart is empty</h4>;
    }

    return (
        <ul className="cart-detail__product-list">
            {cart.map((item) => {
                return (
                    <li key={item.id} className="product-list__cart-item">
                        <ListBookCartItem bookUnit={item} />
                    </li>
                );
            })}
        </ul>
    );
}

export default ListBookCart;
