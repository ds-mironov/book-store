import React, {
    ChangeEventHandler,
    useEffect,
    useState,
} from 'react';

import selectOptions from '../../constants/selectOptions.json';
import {useAppDispatch} from '../../hooks/redux';
import {useInput} from '../../hooks/useInput';
import {addBillingInfoToOrder} from '../../redux/reducers/orderSlice';
import './BillingInfo.css';

export type SelectOption = {
    label: string;
    value: string;
};

const BillingInfo = () => {
    const dispatch = useAppDispatch();
    const firstName = useInput('', {
        isEmpty: true,
    });
    const lastName = useInput('', {
        isEmpty: true,
    });
    const email = useInput('', {
        isEmpty: true,
        minLength: 3,
        isEmail: true,
        maxLength: 16,
    });
    const phoneNumber = useInput('', {
        isEmpty: true,
    });
    const address = useInput('', {
        isEmpty: true,
    });
    const city = useInput('', {
        isEmpty: true,
    });
    const postalCode = useInput('', {
        isEmpty: true,
    });
    const [selected, setSelected] = useState('start');

    const changeHandler: ChangeEventHandler<HTMLSelectElement> = (e) => {
        setSelected(e.target.value);
    };

    useEffect(() => {
        const isFilledFields =
            email.inputValid &&
            firstName.value &&
            lastName.value &&
            email.value &&
            phoneNumber.value &&
            address.value &&
            selected !== 'start' &&
            postalCode.value;

        if (isFilledFields) {
            dispatch(
                addBillingInfoToOrder({
                    firstName: firstName.value,
                    lastName: lastName.value,
                    email: email.inputValid ? email.value : '',
                    phoneNumber: phoneNumber.value,
                    address: address.value,
                    city: city.value,
                    country: selected,
                    postalCode: postalCode.value,
                }),
            );
        } else {
            dispatch(addBillingInfoToOrder(null));
        }
    }, [
        firstName.value,
        lastName.value,
        email.value,
        email.inputValid,
        phoneNumber.value,
        address.value,
        city.value,
        selected,
        postalCode.value,
    ]);

    return (
        <>
            <div className="requisites-point__billing-forms">
                <div className="billing-forms__input-block">
                    <label className="input-block__label" htmlFor="input-name">
                        First name
                    </label>
                    <input
                        className="input-block__input"
                        value={firstName.value}
                        type="text"
                        id="input-name"
                        placeholder="First name"
                        name="firstname"
                        tabIndex={1}
                        onChange={firstName.handleChange}
                    />
                </div>
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="input-lastname">
                        Last name
                    </label>
                    <input
                        className="input-block__input"
                        value={lastName.value}
                        onChange={lastName.handleChange}
                        type="text"
                        id="input-lastname"
                        placeholder="Last name"
                        name="lastname"
                        tabIndex={2}
                    />
                </div>
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="billing-email">
                        Email
                    </label>
                    <input
                        className="input-block__input"
                        type="email"
                        value={email.value}
                        id="billing-email"
                        placeholder="Enter your email"
                        name="email"
                        tabIndex={3}
                        autoComplete="email"
                        onChange={email.handleChange}
                        onBlur={email.handleBlur}
                    />
                    {email.isDirty && email.emailError && (
                        <div className="field__warning">
                            <span>Incorrect email</span>
                        </div>
                    )}
                </div>
                <div className="billing-forms__input-block">
                    <label className="input-block__label" htmlFor="input-phone">
                        Phone number
                    </label>
                    <input
                        className="input-block__input"
                        value={phoneNumber.value}
                        onChange={phoneNumber.handleChange}
                        type="tel"
                        maxLength={12}
                        id="input-phone"
                        placeholder="Phone number"
                        name="phone"
                        tabIndex={4}
                    />
                </div>
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="input-address">
                        Address
                    </label>
                    <input
                        className="input-block__input"
                        value={address.value}
                        onChange={address.handleChange}
                        type="text"
                        id="input-address"
                        placeholder="Address"
                        name="address"
                        tabIndex={5}
                    />
                </div>
                <div className="billing-forms__input-block">
                    <label className="input-block__label" htmlFor="input-city">
                        Town / City
                    </label>
                    <input
                        className="input-block__input"
                        value={city.value}
                        onChange={city.handleChange}
                        type="text"
                        id="input-city"
                        placeholder="Town or City"
                        name="city"
                        tabIndex={6}
                    />
                </div>
                <div className="billing-forms__input-block">
                    <label className="input-block__label" htmlFor="state">
                        State / Country
                    </label>
                    <div className="input-block__select">
                        <select
                            value={selected}
                            className="select__state"
                            id="state"
                            tabIndex={7}
                            onChange={changeHandler}>
                            <option value="start" disabled hidden>
                                Choose a state or Country
                            </option>
                            {selectOptions.map((option: SelectOption) => (
                                <option key={option.value} value={option.value}>
                                    {option.label}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="input-postal">
                        ZIP / Postal code
                    </label>
                    <input
                        className="input-block__input"
                        value={postalCode.value}
                        onChange={postalCode.handleChange}
                        type="text"
                        id="input-postal"
                        placeholder="Postal code or ZIP"
                        name="postal-code"
                        tabIndex={8}
                    />
                </div>
            </div>
        </>
    );
};

export default BillingInfo;
