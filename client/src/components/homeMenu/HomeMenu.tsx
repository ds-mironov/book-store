import React from 'react';

import {IBook} from '../../types/Book';
import HomeMenuItem from '../homeMenuItem/HomeMenuItem';
import {HomeMenuButton} from '../UI/homeMenuButton/HomeMenuButton';
import './HomeMenu.css';

type HomeMenuProps = {
    books: IBook[];
};

function HomeMenu({books}: HomeMenuProps) {
    return (
        <div className="category__menu">
            <h3 className="menu__title">Best selling books</h3>
            <ul className="menu__category-list">
                {books.slice(0, 5).map((book) => {
                    return (
                        <li
                            key={book['ISBN']}
                            className="category-list__menu-item">
                            <HomeMenuItem book={book} />
                        </li>
                    );
                })}
            </ul>
            <HomeMenuButton />
        </div>
    );
}

export default HomeMenu;
