import React from 'react';

import {IAuthor} from '../../types/Author';
import AuthorGridItem from '../authorGridItem/AuthorGridItem';
import ErrorIndicator from '../shared/errorIndicator/ErrorIndicator';
import Spinner from '../UI/spinner/Spinner';
import '../bookGrid/BookGrid.css';

type AuthorGridProps = {
    category: IAuthor[];
    loading: boolean;
    error: string;
};

function AuthorGrid({category, loading, error}: AuthorGridProps) {
    const renderItems = (list: IAuthor[]) => {
        const items = list.map((item, idx: number) => {
            return (
                <li
                    key={item.id}
                    className="product-grid__product-unit"
                    tabIndex={idx + 5}>
                    <AuthorGridItem author={item} />
                </li>
            );
        });

        return <ul className="category__product-grid">{items}</ul>;
    };

    const items = renderItems(category);

    const errorIndicator = error ? <ErrorIndicator /> : null;
    const spinner = loading ? <Spinner /> : null;
    const content = !(loading || error) ? items : null;

    return (
        <>
            {errorIndicator}
            {spinner}
            {content}
        </>
    );
}

export default AuthorGrid;
