import React, { ChangeEvent } from 'react';
import './Checkbox.css';

type CheckboxProps = {
    changeAddressHandler(e: ChangeEvent<HTMLInputElement>): void;
    children: string;
};

const Checkbox = ({children, changeAddressHandler}: CheckboxProps) => {
    return (
        <div className="requisites-point__checkbox-wrapper">
            <input
                className="checkbox-wrapper__checkbox"
                type="checkbox"
                id="input-checkbox"
                onChange={changeAddressHandler}
            />
            <div className="checkbox-wrapper__mark">{''}</div>
            <label className="checkbox-wrapper__label" htmlFor="input-checkbox">
                {children}
            </label>
        </div>
    );
};

export default Checkbox;
