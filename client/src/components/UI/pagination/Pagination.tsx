import React from 'react';

import {nanoid} from '@reduxjs/toolkit';

import {usePagination} from '../../../hooks/usePagination';
import './Pagination.css';

type PaginationProps = {
    totalPages: number;
    page: number;
};

function Pagination({totalPages, page}: PaginationProps) {
    const pages = usePagination(totalPages);

    return (
        <ul className="choice-page__page-list">
            {pages.map((p) => (
                <li
                    key={nanoid()}
                    className={
                        page === p
                            ? 'page-list__page-number page-list__page-number_active'
                            : 'page-list__page-number'
                    }>
                    {p}
                </li>
            ))}
        </ul>
    );
}

export default Pagination;
