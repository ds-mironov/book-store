import React from 'react';

import {useNavigate} from 'react-router-dom';

import './HomeMenuButton.css';
import rightIcon from '../../../assets/icons/ic-chevron-right.svg';

export function HomeMenuButton() {
    const navigate = useNavigate();

    return (
        <button
            className="menu__button-more"
            onClick={() => navigate('/books')}>
            More books
            <img
                className="button-more__image"
                src={rightIcon}
                alt="More books"
            />
        </button>
    );
}
