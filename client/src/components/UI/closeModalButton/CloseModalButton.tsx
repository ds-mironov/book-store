import React from 'react';

import './CloseModalButton.css';
import closeSimpleIcon from '../../../assets/icons/ic-actions-close-simple.svg';

type CloseModalButtonProps = {
    onCloseModal: () => void;
};

function CloseModalButton({onCloseModal}: CloseModalButtonProps) {
    return (
        <button
            type="button"
            className="title-block__close-btn"
            onClick={onCloseModal}>
            Close
            <img
                className="close-btn__icon"
                src={closeSimpleIcon}
                alt="Close button"
            />
        </button>
    );
}

export default CloseModalButton;
