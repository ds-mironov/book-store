import React from 'react';
import './Button.css';

type ButtonProps = {
    hasBeforeIcon?: boolean;
    styleClass?: string;
    disabled?: boolean;
    handleClick?: () => void;
    handleSubmitClick?: (e: React.SyntheticEvent) => Promise<void>;
    children: React.ReactNode;
    tabIndex?: number;
};

export function Button({
    hasBeforeIcon = false,
    styleClass = '',
    disabled = false,
    handleClick,
    handleSubmitClick,
    children,
}: ButtonProps) {
    const clazz = styleClass
        ? `price-buy__buy-btn price-buy__buy-btn_${styleClass}`
        : 'price-buy__buy-btn';

    if (hasBeforeIcon) {
        return (
            <button
                className={clazz}
                onClick={handleClick || handleSubmitClick}
                disabled={disabled}>
                {children}
            </button>
        );
    }

    return (
        <button
            className={clazz}
            onClick={handleClick || handleSubmitClick}
            disabled={disabled}>
            {children}
        </button>
    );
}
