import React from 'react';
import './Popup.css';

type PopupProps = {
    active: boolean;
    setActive: (isActive: boolean) => void;
    children: React.ReactNode;
};

function Popup({active, setActive, children}: PopupProps) {
    return (
        <div
            className={active ? 'modal active' : 'modal'}
            onClick={() => setActive(false)}>
            <div
                className={active ? 'modal__content active' : 'modal__content'}
                onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
}

export default Popup;
