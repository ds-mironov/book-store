import React, {useEffect, useState} from 'react';

import closeSimpleIcon from '../../assets/icons/ic-actions-close-simple.svg';
import {useAppDispatch} from '../../hooks/redux';
import {
    bookChangedQuantityCart,
    bookRemovedFromCart,
} from '../../redux/reducers/shoppingCartSlice';
import {ICartItem} from '../../types/ShoppingCart';
import QuantitySelector from '../quantitySelector/QuantitySelector';
import './ListBookCartItem.css';

type ListBookCartItemProps = {
    bookUnit: ICartItem;
};

function ListBookCartItem({bookUnit}: ListBookCartItemProps) {
    const dispatch = useAppDispatch();
    const {id, title, count, image, total} = bookUnit;
    const [quantity, setQuantity] = useState(`${count}`);

    useEffect(() => {
        dispatch(bookChangedQuantityCart({bookUnit, quantity}));
    }, [quantity]);

    const onChangeQuantity: React.ChangeEventHandler<HTMLInputElement> = (
        e,
    ) => {
        if (+e.target.value >= 0) {
            setQuantity(e.target.value);
        }
    };

    return (
        <div className="cart-item__product-detail">
            <div className="product-detail__image-actions">
                <img className="image-actions__image" src={image} alt={title} />
                <button
                    className="image-actions__remove-btn"
                    onClick={() => dispatch(bookRemovedFromCart(id))}>
                    <img
                        className="remove-btn__icon"
                        src={closeSimpleIcon}
                        alt="Remove book"
                    />
                    Remove
                </button>
            </div>
            <div className="product-detail__detail-actions">
                <h3 className="detail-actions__title">{title}</h3>
                <div className="detail-actions__product-detail">
                    <div className="product-detail__product-data">
                        <ul className="product-data__title-list">
                            <li className="title-list__elem">SKU</li>
                            <li className="title-list__elem">Category</li>
                        </ul>
                        <ul className="product-data__value-list">
                            <li className="value-list__elem">{id}</li>
                            <li className="value-list__elem">Books</li>
                        </ul>
                    </div>
                </div>
                <div className="product-data__price-choice">
                    <span className="price-choice__price">{total} EUR</span>
                    <div className="price-choice__choice-quantity">
                        <QuantitySelector
                            styleClass="cart"
                            quantity={quantity}
                            handleChange={onChangeQuantity}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListBookCartItem;
