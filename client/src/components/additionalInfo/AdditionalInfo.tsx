import React, {createRef} from 'react';

import { useAppDispatch } from '../../hooks/redux';
import {addAdditionalInfoToOrder} from '../../redux/reducers/orderSlice';
import './AdditionalInfo.css';

function AdditionalInfo() {
    const textarea = createRef<HTMLTextAreaElement>();
    const dispatch = useAppDispatch();

    const handleChangeTextarea = () => {
        if (textarea.current?.value) {
            dispatch(addAdditionalInfoToOrder(textarea.current.value));
        }
    };

    return (
        <div className="requisites-point__textarea-block">
            <label className="textarea-block__label" htmlFor="textarea">
                Order notes
            </label>
            <textarea
                className="textarea-block__textarea"
                id="textarea"
                ref={textarea}
                defaultValue="Need a specific delivery day? Sending a gift? Let’s say ..."
                name="addition"
                onChange={handleChangeTextarea}
            />
        </div>
    );
}

export default AdditionalInfo;
