import React from 'react';

import { Link } from 'react-router-dom';

import { useAppDispatch } from '../../hooks/redux';
import { bookAddedToCart } from '../../redux/reducers/shoppingCartSlice';
import { IBook } from '../../types/Book';
import { Button } from '../UI/button/Button';
import './BookGridItem.css';

type BookGridItemProps = {
    book: IBook
    text: string
}

function BookGridItem({ book, text }: BookGridItemProps) {
    const {
        ISBN,
        title,
        summary,
        image,
        price: { displayValue, currency },
    } = book;
    const dispatch = useAppDispatch();

    const handleBookAddedToCart = () => dispatch(bookAddedToCart({ book }));

    return (
        <>
            <Link to={`/book/${ISBN}`}>
                <img className="product-unit__image" src={image} alt="title" />
            </Link>
            <div className="product-unit__info">
                <h4 className="info__title">{title}</h4>
                <p className="info__description">{`${summary.slice(
                    0,
                    39
                )}...`}</p>
            </div>
            <div className="info__price-buy">
                <p className="price-buy__price">{`${displayValue} ${currency}`}</p>
                <Button handleClick={handleBookAddedToCart}>{text}</Button>
            </div>
        </>
    );
}

export default BookGridItem;
