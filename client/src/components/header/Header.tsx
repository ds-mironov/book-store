import React from 'react';

import {Link} from 'react-router-dom';

import userIcon from '../../assets/icons/ic-actions-user.svg';
import basketIcon from '../../assets/icons/ic-basket.svg';
import HeaderPanelInfo from '../headerPanelInfo/HeaderPanelInfo';
import './Header.css';

type HeaderProps = {
    onToggleAuthForm: () => void;
    onToggleCart: () => void;
    children: React.ReactNode;
    quantityCartItems: number;
};

function Header({
    onToggleAuthForm,
    onToggleCart,
    children,
    quantityCartItems,
}: HeaderProps) {
    return (
        <header className="container__header">
            <div className="header__user-panel">
                <HeaderPanelInfo />
                <hr className="user-panel__separator" />
                <div className="user-panel__actions-block">
                    <Link to="/" className="actions-block__logo">
                        BookStore
                    </Link>
                    {children}
                    <div className="actions-block__actions">
                        <button
                            className="actions__account-button"
                            onClick={onToggleAuthForm}>
                            <img
                                className="account-button__icon"
                                src={userIcon}
                                alt="Account"
                            />
                        </button>
                        <div className="actions__basket-wrapper">
                            <button
                                className="basket-wrapper__button"
                                onClick={onToggleCart}>
                                {''}
                            </button>
                            <div className="basket-wrapper__circle">
                                <span className="circle__counter">
                                    {quantityCartItems}
                                </span>
                            </div>
                            <img
                                className="basket-wrapper__icon"
                                src={basketIcon}
                                alt="Basket"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default Header;
