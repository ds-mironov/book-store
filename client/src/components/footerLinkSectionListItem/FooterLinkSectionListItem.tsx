import React from 'react';

import {Link} from 'react-router-dom';
import './FooterLinkSectionListItem.css';

type FooterLinkSectionListItemProps = {
    title: string;
    link: string;
    external: boolean;
};

function FooterLinkSectionListItem({
    title,
    link,
    external,
}: FooterLinkSectionListItemProps) {
    const externalLink = (
        <a
            href={`${link}`}
            className="item__touch-link"
            target="_blank"
            rel="nofollow noopener noreferrer">
            {title}
        </a>
    );

    const innerLink = (
        <Link to={link} className="item__touch-link">
            {title}
        </Link>
    );

    return <>{external ? externalLink : innerLink}</>;
}

export default FooterLinkSectionListItem;
