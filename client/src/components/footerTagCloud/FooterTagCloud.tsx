import React from 'react';

import {IBook} from '../../types/Book';
import FooterTagCloudItem from '../footerTagCloudItem/FooterTagCloudItem';
import './FooterTagCloud.css';

type FooterTagCloudProps = {
    bookList: IBook[];
};

function FooterTagCloud({bookList}: FooterTagCloudProps) {
    return (
        <div className="footer__tags-block">
            <h3 className="tags-block__title">Book tags</h3>
            <ul className="tags-block__list">
                {bookList.slice(0, 15).map((item) => {
                    return (
                        <li key={item['ISBN']} className="list__tag">
                            <FooterTagCloudItem
                                title={item.title}
                                id={item['ISBN']}
                            />
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

export default FooterTagCloud;
