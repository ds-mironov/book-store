import React, {ChangeEvent} from 'react';
import './QuantitySelector.css';

type QuantitySelectorProps = {
    quantity: string;
    handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
    styleClass?: string;
};

function QuantitySelector({
    quantity,
    handleChange,
    styleClass = '',
}: QuantitySelectorProps) {
    const clazz = styleClass
        ? `choice-quantity__select-quantity choice-quantity__select-quantity_${styleClass}`
        : 'choice-quantity__select-quantity';

    return (
        <div className={clazz}>
            <input
                className="select-quantity__input"
                onChange={handleChange}
                value={quantity}
            />
            <select className="select-quantity__type" name="categories">
                <option value="1">Pcs</option>
                <option value="2">Box</option>
            </select>
        </div>
    );
}

export default QuantitySelector;
