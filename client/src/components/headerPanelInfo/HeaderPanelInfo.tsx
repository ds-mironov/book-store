import React from 'react';

import {Link} from 'react-router-dom';
import './HeaderPanelInfo.css';

function HeaderPanelInfo() {
    return (
        <div className="user-panel__info">
            <ul className="info__list-left">
                <li className="list-left__item">
                    <Link to="*" className="item__info-links">
                        Chat with us
                    </Link>
                </li>
                <li className="list-left__item">
                    <a
                        className="item__info-links item__info-links_font_black"
                        href="tel:+420336775664">
                        +420 336 775 664
                    </a>
                </li>
                <li className="list-left__item">
                    <a
                        className="item__info-links item__info-links_black-font"
                        href="mailto:info@bookstore.site">
                        info@bookstore.site
                    </a>
                </li>
            </ul>
            <ul className="info__list-right">
                <li className="list-right__item">
                    <Link to="*" className="item__info-links">
                        Blog
                    </Link>
                </li>
                <li className="list-right__item">
                    <Link to="*" className="item__info-links">
                        About Us
                    </Link>
                </li>
                <li className="list-right__item">
                    <Link to="*" className="item__info-links">
                        Carriers
                    </Link>
                </li>
            </ul>
        </div>
    );
}

export default HeaderPanelInfo;
