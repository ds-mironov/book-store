import React, {useCallback, useEffect, useState} from 'react';

import {useDispatch} from 'react-redux';

import bitcoinLogo from '../../assets/logo/bitcoin.svg';
import paypalLogo from '../../assets/logo/paypal.svg';
import visaMCLogo from '../../assets/logo/visa-mastercard.svg';
import {addPaymentToOrder} from '../../redux/reducers/orderSlice';
import {ICreditCard} from '../../types/Order';
import CreditCardForm from '../creditCardForm/CreditCardForm';
import './PaymentMethod.css';

function PaymentMethod() {
    const [method, setMethod] = useState<string>('creditCard');
    const [card, setCard] = useState<ICreditCard | null>(null);
    const dispatch = useDispatch();

    const choicePayment: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setMethod(e.target.value);
    };

    const getCardDetails = useCallback((card: ICreditCard | null) => {
        setCard(card);
    }, []);

    useEffect(() => {
        if (method === 'creditCard' && card) {
            dispatch(addPaymentToOrder({method, card}));
        } else {
            dispatch(addPaymentToOrder(null));
        }
    }, [method, card]);

    return (
        <>
            <div className="payment-form__requisites-point">
                <label
                    className={
                        method !== 'creditCard'
                            ? 'requisites-point__payment-point'
                            : 'requisites-point__payment-point requisites-point__payment-point_background_white'
                    }>
                    <p className="payment-point__form-detail">
                        <span className="form-detail__title">Credit card</span>
                        <img
                            className="form-detail__logo"
                            src={visaMCLogo}
                            alt="Visa / Mastercard"
                        />
                    </p>
                    <input
                        className="payment-point__input"
                        value="creditCard"
                        checked={method === 'creditCard'}
                        onChange={choicePayment}
                        type="radio"
                        name="payment"
                    />
                    <span className="payment-point__checkmark">{}</span>
                </label>
                {method === 'creditCard' && (
                    <CreditCardForm getCardDetails={getCardDetails} />
                )}
            </div>
            <div className="payment-form__requisites-point">
                <label
                    className={
                        method !== 'paypal'
                            ? 'requisites-point__payment-point'
                            : 'requisites-point__payment-point requisites-point__payment-point_background_white'
                    }>
                    <p className="payment-point__form-detail">
                        <span className="form-detail__title">PayPal</span>
                        <img
                            className="form-detail__logo"
                            src={paypalLogo}
                            alt="DHL"
                        />
                    </p>
                    <input
                        className="payment-point__input"
                        value="paypal"
                        onChange={choicePayment}
                        type="radio"
                        name="payment"
                    />
                    <span className="payment-point__checkmark">{}</span>
                </label>
                {method === 'paypal' && (
                    <div className="requisites-point__info-block">
                        <span className="info-block__message">
                            Sorry, this payment method is not yet supported on
                            the site!
                        </span>
                    </div>
                )}
            </div>
            <div className="payment-form__requisites-point">
                <label
                    className={
                        method !== 'bitcoin'
                            ? 'requisites-point__payment-point'
                            : 'requisites-point__payment-point requisites-point__payment-point_background_white'
                    }>
                    <p className="payment-point__form-detail">
                        <span className="form-detail__title">Bitcoin</span>
                        <img
                            className="form-detail__logo"
                            src={bitcoinLogo}
                            alt="Fedex"
                        />
                    </p>
                    <input
                        className="payment-point__input"
                        value="bitcoin"
                        onChange={choicePayment}
                        type="radio"
                        name="payment"
                    />
                    <span className="payment-point__checkmark">{}</span>
                </label>
                {method === 'bitcoin' && (
                    <div className="requisites-point__info-block">
                        <span className="info-block__message">
                            Sorry, this payment method is not yet supported on
                            the site!
                        </span>
                    </div>
                )}
            </div>
        </>
    );
}

export default PaymentMethod;
