import React, {SyntheticEvent, useContext, useState} from 'react';

import securityIcon from '../../assets/icons/ic-security-savety.svg';
import {AuthContext} from '../../contexts/AuthContext';
import {useAppSelector} from '../../hooks/redux';
import {useHttp} from '../../hooks/useHttp';
import {IOrderState} from '../../types/Order';
import {ICartItem} from '../../types/ShoppingCart';
import {Button} from '../UI/button/Button';
import './Confirmaiton.css';

type ConfirmationProps = {
    cart: ICartItem[];
};

function Confirmation({cart}: ConfirmationProps) {
    const auth = useContext(AuthContext);
    const {loading, request} = useHttp();
    const order = useAppSelector((state) => state.order);
    const {billingInfo, delivery, payment} = order as IOrderState;
    const [confirmed, setConfirmed] = useState({
        subscription: false,
        agreement: false,
    });
    const isValidOrder =
        cart.length !== 0 &&
        billingInfo &&
        delivery &&
        payment &&
        confirmed.agreement;

    const handleCheckedChange: React.ChangeEventHandler<HTMLInputElement> = (
        event,
    ) => {
        setConfirmed({
            ...confirmed,
            [event.target.name]: event.target.checked,
        });
    };

    const handleSubmitOrder = async (e: SyntheticEvent) => {
        e.preventDefault();

        if (isValidOrder) {
            const orderDetails = {
                orderNumber: Date.now(),
                cart,
                ...(order as IOrderState),
                subscription: confirmed.subscription,
                created: new Date(),
            };

            await request(
                'api/order/create',
                'POST',
                {orderDetails},
                {
                    Authorization: `Bearer ${auth?.token}`,
                },
            );
        }
    };

    return (
        <>
            <div className="requisites-point__confirmation">
                <div className="requisites-point__checkbox-wrapper">
                    <input
                        className="checkbox-wrapper__checkbox"
                        type="checkbox"
                        id="input-checkbox"
                        name="subscription"
                        onChange={handleCheckedChange}
                    />
                    <div className="checkbox-wrapper__mark">{''}</div>
                    <label
                        className="checkbox-wrapper__label"
                        htmlFor="input-checkbox">
                        I agree with sending an Marketing and newsletter emails.
                        No spam, promissed!
                    </label>
                </div>
                <div className="requisites-point__checkbox-wrapper">
                    <input
                        className="checkbox-wrapper__checkbox"
                        type="checkbox"
                        id="input-checkbox"
                        name="agreement"
                        onChange={handleCheckedChange}
                    />
                    <div className="checkbox-wrapper__mark">{''}</div>
                    <label
                        className="checkbox-wrapper__label"
                        htmlFor="input-checkbox">
                        I agree with our terms and conditions and privacy
                        policy.
                    </label>
                </div>
            </div>
            <Button
                styleClass="checkout"
                handleSubmitClick={handleSubmitOrder}
                disabled={loading || !isValidOrder}>
                Complete order
            </Button>
            <div className="requisites-point__safe-data">
                <img
                    className="requisites-point__security-shield"
                    src={securityIcon}
                    alt="Safely"
                />
                <p className="requisites-point__title">
                    All your data are safe
                </p>
                <p className="requisites-point__manifest">
                    We are using the most advanced security to provide you the
                    best experience ever.
                </p>
            </div>
        </>
    );
}

export default Confirmation;
