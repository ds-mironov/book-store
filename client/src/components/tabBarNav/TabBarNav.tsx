import React from 'react';

import classNames from 'classnames';
import './TabBarNav.css';

type TabBarNavProps = {
    navLabel: string;
    className: string;
    onChangeActiveTab(activeTab: string): void;
};

const TabBarNav = ({
    navLabel = 'Tab',
    className = '',
    onChangeActiveTab,
}: TabBarNavProps) => {
    const classes = classNames(className, 'nav-item');

    return (
        <div>
            <button
                className={classes}
                onClick={() => {
                    onChangeActiveTab(navLabel);
                }}>
                {navLabel}
            </button>
        </div>
    );
};

export default TabBarNav;
