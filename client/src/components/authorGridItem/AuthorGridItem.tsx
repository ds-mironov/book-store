import React from 'react';

import { Link } from 'react-router-dom';

import { IAuthor } from '../../types/Author';
import '../bookGridItem/BookGridItem.css';

type AuthorGridItemProps = {
    author: IAuthor
}

function AuthorGridItem({ author }: AuthorGridItemProps) {
    const {
        id,
        name,
        biography,
        image,
    } = author;

    return (
        <>
            <Link to={`/author/${id}`}>
                <img className="product-unit__image" src={image} alt="title" />
            </Link>
            <div className="product-unit__info">
                <h4 className="info__title">{name}</h4>
                <p className="info__description">{`${biography.slice(
                    0,
                    110
                )}...`}</p>
            </div>
        </>
    );
}

export default AuthorGridItem;
