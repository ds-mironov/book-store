import React from 'react';

import {nanoid} from '@reduxjs/toolkit';

import './BookDetailSectionTitleList.css';
import {IFutureTitle} from '../../constants/futureTitleLists';

type BookDetailSectionTitleListProps = {
    list: IFutureTitle[];
};

function BookDetailSectionTitleList({list}: BookDetailSectionTitleListProps) {
    return (
        <ul className="future__title-list">
            {list.map((item) => {
                return (
                    <li key={nanoid()} className="title-list__item">
                        {item.title}
                    </li>
                );
            })}
        </ul>
    );
}

export default BookDetailSectionTitleList;
