import React, {useEffect, useState} from 'react';

import {useDispatch} from 'react-redux';

import selectOptions from '../../constants/selectOptions.json';
import {useInput} from '../../hooks/useInput';
import {addDeliveryInfoToOrder} from '../../redux/reducers/orderSlice';
import {SelectOption} from '../billingInfo/BillingInfo';

function DeliveryAddress() {
    const dispatch = useDispatch();

    const deliveryAddress = useInput('', {
        isEmpty: true,
    });
    const deliveryCity = useInput('', {
        isEmpty: true,
    });
    const deliveryPostalCode = useInput('', {
        isEmpty: true,
    });
    const [selected, setSelected] = useState('start');

    const changeHandler: React.ChangeEventHandler<HTMLSelectElement> = (e) => {
        setSelected(e.target.value);
    };

    useEffect(() => {
        const isFilledFields =
            deliveryAddress.value &&
            deliveryCity.value &&
            deliveryPostalCode.value &&
            selected !== 'start';

        if (isFilledFields) {
            dispatch(
                addDeliveryInfoToOrder({
                    deliveryAddress: deliveryAddress.value,
                    deliveryCity: deliveryCity.value,
                    deliveryState: selected,
                    deliveryPostalCode: deliveryPostalCode.value,
                }),
            );
        }
    }, [
        deliveryAddress.value,
        deliveryCity.value,
        selected,
        deliveryPostalCode.value,
    ]);

    return (
        <>
            <div className="requisites-point__billing-forms">
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="delivery-address">
                        Address
                    </label>
                    <input
                        className="input-block__input"
                        value={deliveryAddress.value}
                        onChange={deliveryAddress.handleChange}
                        type="text"
                        id="delivery-address"
                        placeholder="Address"
                        name="deliveryAddress"
                        tabIndex={9}
                    />
                </div>
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="delivery-city">
                        Town / City
                    </label>
                    <input
                        className="input-block__input"
                        value={deliveryCity.value}
                        onChange={deliveryCity.handleChange}
                        type="text"
                        id="delivery-city"
                        placeholder="Town or City"
                        name="deliveryCity"
                        tabIndex={10}
                    />
                </div>
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="deliveryState">
                        State / Country
                    </label>
                    <div className="input-block__select">
                        <select
                            value={selected}
                            className="select__state"
                            id="deliveryState"
                            tabIndex={11}
                            onChange={changeHandler}>
                            <option value="start" disabled hidden>
                                Choose a state or Country
                            </option>
                            {selectOptions.map((option: SelectOption) => (
                                <option key={option.value} value={option.value}>
                                    {option.label}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className="billing-forms__input-block">
                    <label
                        className="input-block__label"
                        htmlFor="delivery-postal">
                        ZIP / Postal code
                    </label>
                    <input
                        className="input-block__input"
                        value={deliveryPostalCode.value}
                        onChange={deliveryPostalCode.handleChange}
                        type="text"
                        id="delivery-postal"
                        placeholder="Postal code or ZIP"
                        name="delivery-postal"
                        tabIndex={12}
                    />
                </div>
            </div>
        </>
    );
}

export default DeliveryAddress;
