import React from 'react';

import {nanoid} from '@reduxjs/toolkit';

import {IBook} from '../../types/Book';
import CategoryMenuItem from '../categoryMenuItem/CategoryMenuItem';
import './CategoryMenu.css';

type CategoryMenuProps = {
    books: IBook[];
};

interface T {
    [index: string]: number
}

function CategoryMenu({books}: CategoryMenuProps) {
    const renderItems = (list: IBook[]) => {
        return Object.entries(
            list.reduce((acc: T, book: IBook) => {
                const {author} = book;
                acc[author] = (acc[author] || 0) + 1;
                return acc;
            }, {}),
        ).map((item) => {
            return (
                <li
                    key={nanoid()}
                    className="category-list__menu-item category-list__menu-item_with-quantity">
                    <CategoryMenuItem name={item[0]} count={item[1]} />
                </li>
            );
        });
    };

    const items = renderItems(books);

    return (
        <div className="category__menu">
            <h3 className="menu__title">Authors</h3>
            <ul className="menu__category-list">{items}</ul>
        </div>
    );
}

export default CategoryMenu;
