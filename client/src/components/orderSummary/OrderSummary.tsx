import React from 'react';

import {useAppSelector} from '../../hooks/redux';
import {ICartItem} from '../../types/ShoppingCart';
import {decimalAdjustRound} from '../../utils/helpers';
import ListBookCart from '../listBookCart/ListBookCart';
import './OrderSummary.css';

type OrderSummaryProps = {
    cart: ICartItem[];
};

function OrderSummary({cart}: OrderSummaryProps) {
    const subtotal = decimalAdjustRound(
        cart.reduce((acc, currentItem) => {
            return acc + currentItem.total;
        }, 0),
        -2,
    );
    const tax = decimalAdjustRound(subtotal * 0.17, -2);
    const deliveryPrice =
        useAppSelector((state) => state.order.delivery?.price) || 0;
    const totalOrder = decimalAdjustRound(subtotal + tax + deliveryPrice, -2);

    return (
        <div className="order__basket-block">
            <div className="basket-block__order-title">
                <h3 className="order-title__title">Order Summary</h3>
                <p className="order-title__help">
                    Price can change depending on shipping method and taxes of
                    your state.
                </p>
            </div>
            <ListBookCart cart={cart} />
            <div className="basket__calculation">
                <div className="calculation__check-points">
                    <div className="check-points__points-item">
                        <div className="points-item__item">Subtotal</div>
                        <div className="points-item__item">{subtotal} EUR</div>
                    </div>
                    <div className="check-points__points-item">
                        <div className="points-item__item">Tax</div>
                        <div className="points-item__item">17% {tax} EUR</div>
                    </div>
                    <div className="check-points__points-item">
                        <div className="points-item__item">Shipping</div>
                        <div className="points-item__item">
                            {deliveryPrice} EUR
                        </div>
                    </div>
                </div>
                <div className="calculation__promo-code">
                    <input
                        className="promo-code__input"
                        type="text"
                        placeholder="Apply promo code"
                    />
                    <button className="promo-code__button">Apply now</button>
                </div>
            </div>
            <div className="basket__total-order">
                <div className="total-order__detail-block">
                    <p className="detail-block__total">Total Order</p>
                    <p className="detail-block__delivery">
                        Guaranteed delivery day: June 12, 2020
                    </p>
                </div>
                <div className="total-order__value">{totalOrder} EUR</div>
            </div>
        </div>
    );
}

export default OrderSummary;
