import React from 'react';

import {IBook} from '../../types/Book';
import {ICartItem} from '../../types/ShoppingCart';
import {toggleButtonText} from '../../utils/helpers';
import BookGridItem from '../bookGridItem/BookGridItem';
import ErrorIndicator from '../shared/errorIndicator/ErrorIndicator';
import Spinner from '../UI/spinner/Spinner';
import './BookGrid.css';

type BookGridProps = {
    books?: IBook[];
    loading: boolean;
    error: string | null;
    cart: ICartItem[];
};

function BookGrid({books, loading, error, cart}: BookGridProps) {
    const renderItems = (list: IBook[]) => {
        const items = list.map((item, idx) => {
            const text = toggleButtonText<ICartItem>(
                cart,
                item['ISBN'],
                'Buy now',
            );

            return (
                <li
                    key={item['ISBN']}
                    className="product-grid__product-unit"
                    tabIndex={idx + 5}>
                    <BookGridItem book={item} text={text} />
                </li>
            );
        });

        return <ul className="category__product-grid">{items}</ul>;
    };

    const items = books ? renderItems(books) : null;

    const errorIndicator = error ? <ErrorIndicator /> : null;
    const spinner = loading ? <Spinner /> : null;
    const content = !(loading || error) ? items : null;

    return (
        <>
            {errorIndicator}
            {spinner}
            {content}
        </>
    );
}

export default BookGrid;
