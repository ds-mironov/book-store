import React, {ChangeEventHandler, useState} from 'react';

import searchIcon from '../../assets/icons/ic-actions-search.svg';
import './SearchPanel.css';

type SearchPanelProps = {
    onChangeSearch: (term: string) => void;
};

function SearchPanel({onChangeSearch}: SearchPanelProps) {
    const [term, setTerm] = useState('');

    const onUpdateSearch: ChangeEventHandler<HTMLInputElement> = (e) => {
        const term: string = e.target.value;
        setTerm(term);

        if (term === '') {
            onChangeSearch(term);
        }
    };

    return (
        <div className="actions-block__search">
            <div className="search__select-wrapper">
                <select className="select-wrapper__select" name="categories">
                    <option value="1">All books</option>
                    <option value="2">All authors</option>
                </select>
            </div>
            <input
                className="search__input"
                type="text"
                value={term}
                placeholder="Search Books, authors ..."
                onChange={onUpdateSearch}
            />
            <button
                className="search__button"
                onClick={() => onChangeSearch(term)}>
                <img
                    className="button__search-icon"
                    src={searchIcon}
                    alt="Search"
                />
            </button>
        </div>
    );
}

export default SearchPanel;
