import React from 'react';

import BillingInfo from '../billingInfo/BillingInfo';
import DeliveryAddress from '../deliveryAddress/DeliveryAddress';
import TabBar from '../tabBar/TabBar';
import TabBarItem from '../tabBarItem/TabBarItem';

const Sandbox = () => {
    return (
        <TabBar vertical>
            <TabBarItem label="Billing Info">
                <BillingInfo />
            </TabBarItem>
            <TabBarItem label="Delivery Address">
                <DeliveryAddress />
            </TabBarItem>
            <TabBarItem label="Change password">
                <p>{3}</p>
            </TabBarItem>
        </TabBar>
    );
};

export default Sandbox;
