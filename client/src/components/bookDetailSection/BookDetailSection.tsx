import React from 'react';

import {
    futureTitleList1,
    futureTitleList2,
} from '../../constants/futureTitleLists';
import BookDetailSectionTitleList from '../bookDetailSectionTitleList/BookDetailSectionTitleList';
import './BookDetailSection.css';

type BookDetailSectionProps = {
    isbn: number;
    author: string;
};

function BookDetailSection({isbn, author}: BookDetailSectionProps) {
    return (
        <div className="detail-block__product-detail">
            <div className="product-detail__feature">
                <BookDetailSectionTitleList list={futureTitleList1} />
                <ul className="future__value-list">
                    <li className="value-list__item">{author}</li>
                    <li className="value-list__item">{isbn}</li>
                    <li className="value-list__item">In Stock</li>
                    <li className="value-list__item">Not specified</li>
                </ul>
            </div>
            <div className="product-detail__feature">
                <BookDetailSectionTitleList list={futureTitleList2} />
                <ul className="future__value-list">
                    <li className="value-list__item">Not specified</li>
                    <li className="value-list__item">Not specified</li>
                    <li className="value-list__item">Not specified</li>
                    <li className="value-list__item">Not specified</li>
                </ul>
            </div>
        </div>
    );
}

export default BookDetailSection;
