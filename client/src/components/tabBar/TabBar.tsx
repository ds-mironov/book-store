import React, {
    Children,
    cloneElement,
    ReactElement,
    useEffect,
    useState,
} from 'react';

import classNames from 'classnames';

import {TabBarItemProps} from '../tabBarItem/TabBarItem';
import TabBarNav from '../tabBarNav/TabBarNav';
import './TabBar.css';

interface TabBarProps {
    children: readonly ReactElement<TabBarItemProps>[];
    className?: string;
    vertical: boolean;
}

const TabBar = ({
    children,
    className = '',
    vertical = false,
    ...attrs
}: TabBarProps): JSX.Element => {
    const [currentTab, setCurrentTab] = useState<string | null>(null);
    const classes = classNames('tab-bar', className, {vertical});

    useEffect(() => {
        const activeTab = getChildrenLabels(children)[0];
        setCurrentTab(activeTab);
    }, []);

    const changeActiveTab = (activeTab: string): void => {
        if (currentTab !== activeTab) {
            setCurrentTab(activeTab);
        }
    };

    const getChildrenLabels = (children: readonly ReactElement<TabBarItemProps>[]) => {
        return children.map(({props}) => props.label);
    };

    const renderTabs = () => {
        return getChildrenLabels(children).map((navLabel: string) => {
            return (
                <TabBarNav
                    key={navLabel}
                    navLabel={navLabel}
                    className={classNames({active: currentTab === navLabel})}
                    onChangeActiveTab={changeActiveTab}
                />
            );
        });
    };

    return (
        <div className={classes} {...attrs}>
            <div className="tab-bar__nav">{renderTabs()}</div>
            <div className="tab-container">
                {Children.map(
                    children,
                    (child: ReactElement<TabBarItemProps>) =>
                        cloneElement(child, {activeTab: currentTab}),
                )}
            </div>
        </div>
    );
};

export default TabBar;
