const Order = require('../models/Order');

class OrderService {
    async create(orderDetails, owner) {
        const order = new Order({...orderDetails, owner});
        await order.save();

        return order;
    }
}

module.exports = new OrderService();
