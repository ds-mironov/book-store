const User = require('../models/User');
const bcrypt = require('bcryptjs');

function throwSpecificError(message) {
    throw {
        name: 'SpecificError',
        code: 400,
        message,
    };
}

class AuthService {
    async register(email, password) {
        const dbUser = await User.findOne({email});

        if (dbUser) {
            throwSpecificError('User already exists');
        }

        const hashedPassword = await bcrypt.hash(password, 12);
        const user = new User({email, password: hashedPassword});
        await user.save();

        return user;
    }

    async login(email, password) {
        const user = await User.findOne({email});

        if (!user) {
            throwSpecificError('User is not found');
        }

        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            throwSpecificError(
                'Invalid password, please try to enter the correct password',
            );
        }

        return user;
    }
}

module.exports = new AuthService();
