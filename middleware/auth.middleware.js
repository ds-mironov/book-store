const config = require('config');
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    if (req.method === 'OPTIONS') {
        return next();
    }

    try {
        const token = req.headers.authorization.split(' ')[1]; // Bearer TOKEN

        if (!token) {
            return res.status(401).json({message: 'Нет авторизации'});
        }

        console.log(token);
        req.user = jwt.verify(token, config.get('authConfig.jwtSecretKey'));
        return next();
    } catch (e) {
        return res.status(401).json({message: 'Нет авторизации'});
    }
};
